# Coded, curated, and configured by Rob Muhlestein (@robmuh)
# Rather than provied extensive comments find docs at
# <https://github.com/rwxrob/config/blob/master/README.md>
#
# Pilfer away. If you add or fix something let me know (if you want).
# You can override anything by adding a ~/.bash_{personal,private} if you
# prefer.
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

# shell must be interactive
case $- in
  *i*) ;;
  *) return 
esac

# reset all aliases
unalias -a



[ -z "$OS" ] && export OS=`uname`
case "$OS" in
  Linux)          export PLATFORM=linux ;;
  *indows*)       export PLATFORM=windows ;;
  FreeBSD|Darwin) export PLATFORM=mac ;;
  *)              export PLATFORM=unknown ;;
esac

onmac () {
  [[ $PLATFORM == mac ]]  && return 0
  return 1
} && export -f onmac

onwin () {
  [[ $PLATFORM == windows ]]  && return 0
  return 1
} && export -f onwin

onlinux () {
  [[ $PLATFORM == linux ]]  && return 0
  return 1
} && export -f onlinux

onunknown () {
  [[ PLATFORM == unknown ]]  && return 0
  return 1
} && export -f onunknown


export PATH=\
$HOME/bin:\
$HOME/go/bin:\
$HOME/.cargo/bin:\
/usr/local/opt/coreutils/libexec/gnubin:\
/usr/local/go/bin:\
/usr/local/tinygo/bin:\
/usr/local/bin:\
/usr/local/sbin:\
/usr/games:\
/usr/sbin:\
/usr/bin:\
/snap/bin:\
/sbin:\
/bin

alias path='echo -e ${PATH//:/\\n}' # human readable path


set bell-style none
#set -o noclobber

shopt -s checkwinsize
shopt -s expand_aliases
shopt -s nullglob
shopt -s globstar
shopt -s dotglob
shopt -s extglob

# There really is no reason to *not* use the GNU utilities for your login
# shell but there are lot of reasons *to* use it. The only time it could
# present a problem is using other shell scripts written specifically for
# Mac (darwin/bsd). There are far more scripts written to use the GNU
# utilities in the world. Plus you finger muscle memory will be consistent
# across Linux and Mac.

if [[ $PLATFORM = mac ]]; then
  if ! havecmd gls; then
    echo 'Need to `brew install coreutils` for Mac to work.'
  fi 
fi


# The tmp directory differs significantly between major operating systems
# so will use $TEMP throughout.

# TODO get the windows version (should be %TEMP%)

case "$PLATFORM" in
  windows) ;;
  *) export TEMP=/tmp
esac

mktempname () {
  echo "${1:-temp}.$$.$(tstamp)"
} && export -f mktempname

mktemppath () {
  [[ -z "$TEMP" ]] && return 1
  declare name=$(mktempname "$*")
  echo "$TEMP/$name"
} && export -f mktemppath

mktempdir () {
  declare path=$(mktemppath "$*")
  mkdir "$path"
  echo $path
} && export -f mktempdir

mktempfile () {
  declare path=$(mktemppath "$*")
  touch "$name"
  echo $name
} && export -f mktempfile
# Directory (cd) shortcuts. Using associative arrays pointing to
# environment variable names cuz names might not always match (otherwise
# would just ${foo^^} them). Might want to use a different env variable
# name at some point.

declare -A directories=(
  [repos]=REPOS
  [config]=CONFIG
  [personal]=PERSONAL
  [private]=PRIVATE
  [tokens]=TOKENS
  [downloads]=DOWNLOADS
  [desktop]=DESKTOP
  [pictures]=PICTURES
  [videos]=VIDEOS
  [images]=DISKIMAGES
  [vmachines]=VMACHINES
  [readme]=README
  [documents]=DOCUMENTS
)

for k in "${!directories[@]}"; do 
  v=${directories[$k]}
  alias $k='\
    if [[ -n "$'$v'" ]];\
      then cd "$'$v'";\
    else\
      warnln "\`\$'$v'\` not set. Consider adding to ~/.bash_{personal,private}.";\
    fi'
done

# Detect reasonable defaults (override in .bash_private). You'll want to
# set CONFIG in your PERSONAL or PRIVATE locations. See the following for
# examples of how to do this:
#
#    https://github.com/rwxrob/config-personal-sample
#    https://github.com/rwxrob/config-private-sample

declare -A defaults=(
  [DOWNLOADS]=~/Downloads
  [REPOS]=~/Repos
  [DESKTOP]=~/Desktop
  [DOCUMENTS]=~/Documents
  [README]=~/Documents/README   # README WorldPress content
  [PICTURES]=~/Pictures
  [VIDEOS]=~/Videos
  [DISKIMAGES]=~/DiskImages     # linux, arch, raspberrian, etc.
  [VMACHINES]=~/VMachines       # vmware, virtual box, etc.
  [TRASH]=~/Trash               # trash (see trash)
) 

for k in "${!defaults[@]}"; do
  v=${defaults[$k]}
  export $k=$v
done

# Graphic web site shortcuts.

declare -A sites=(
  [github]=github.com
  [gitlab]=gitlab.com
  [protonmail]=protonmail.com
  [skilstak]=skilstak.io
  [dockhub]=hub.docker.com
  [twitter]=twitter.com
  [medium]=medium.com
  [reddit]=reddit.com
  [patreon]=patreon.com
  [paypal]=paypal.com
  [hackerone]=hackerone.com
  [bugcrowd]=bugcrowd.com
  [synack]=synack.com
  [bls]=bls.gov
  [youtube]=youtube.com
  [twitch]=twitch.com
  [vimeo]=vimeo.com
  [emojipedia]=emojipedia.com
  [netflix]=netflix.com
  [amazon]=amazon.com
)

for shortcut in "${!sites[@]}"; do 
  url=${sites[$shortcut]}
  alias $shortcut="open https://$url &>/dev/null"
done

alias '?'=duck
alias '??'=google

############################# Edit Shortcuts #############################

export EDITOR=vi
export VISUAL=vi
export EDITOR_PREFIX=vi

export VIMSPELL=(~/.vim/spell/*.add)
declare personalspell=(~/.vimpersonal/spell/*.add)
[[ -n "$personalspell" ]] && VIMSPELL=$personalspell
declare privatespell=(~/.vimprivate/spell/*.add)
[[ -n $privatespell ]] && VIMSPELL=$privatespell

# Commonly edited files.

declare -A edits=(
  [bashrc]=~/.bashrc
  [personal]=~/.bash_personal
  [private]=~/.bash_private
  [profile]=~/.profile
  [spell]=$VIMSPELL
)

for cmd in "${!edits[@]}"; do 
  path=${edits[$cmd]}
  case $PLATFORM in
    *) alias $EDITOR_PREFIX$cmd="$EDITOR '$path';warnln 'Make sure you git commit your changes (if needed).'" ;;
  esac
done
############################## Markdown-ish ##############################

# Minimal packrat parsing expression grammar (PEG):
# 
#   Mark      <- Span* !.
#   Span      <- Plain / StrongEm / Strong / Emphasis / Link / Code
#   StrongEm  <- '***' .* '***'
#   StrongEm  <- '***' .* '***'
#   Strong    <- '**' .* '**'
#   Emphasis  <- '*' .* '*'
#   Link      <- '<' .* '>'
#   Code      <- '`' .* '`'
#   Plain     <- DQUOTE / QUOTE / LARROW / RARROW / ELLIPSIS / .*
#   DQUOTE    <- '"'
#   QUOTE     <- "'"
#   LARROW    <- " <- " 
#   RARROW    <- " -> " 
#   ELLIPSIS  <- '...'
# 
# There's no character escape support because it isn't needed for most
# all use cases here. 

export md_strongem=$red     #  ***strongem***
export md_strong=$yellow    #  **strong**
export md_emphasis=$magen   #  *emphasis*
export md_code=$cyan        #  `code`
export md_plain=''          #  plain

mark () {
  declare inemphasis instrong instrongem incode indquote inquote inlink i
  declare defsol1="$normal"
  declare defsol2="$normalbg"

  # Detect default colors (fore, bg) from optional first and second
  # arguments. 

  issol "$1" && defsol1="$1" && shift
  issol "$1" && defsol2="$1" && shift
  declare defsol="$defsol1$defsol2"
  echo -n "$defsol"

  # The main content is almost always within single quotes to prevent
  # shell expansions, but we'll combine the remaining arguments anyway for
  # convenience (like echo does).

  declare buf=$*

  # recursive descent parser

  for (( i=0; i<${#buf}; i++ )); do 

    # ***strongem***

    if [[ "${buf:$i:3}" = '***' ]]; then
      if [[ -z "$instrongem" ]]; then
        echo -n "$md_strongem"
        #echo -n '<strongem>'
        instrongem=1
      else
        #echo -n '</strongem>'
        echo -n $defsol
        instrongem=''
      fi
      i=$[i+2]
      continue
    fi

    # **strong**

    if [[ "${buf:$i:2}" = '**' ]]; then
      if [[ -z "$instrong" ]]; then
        echo -n "$md_strong"
        #echo -n '<strong>'
        instrong=y
      else
        #echo -n '</strong>'
        echo -n $defsol
        instrong=''
      fi
      i=$[i+1]
      continue
    fi

    # *emphasis*

    if [[ "${buf:$i:1}" = '*' ]]; then
      if [[ -z "$inemphasis" ]]; then
        echo -n "$md_emphasis"
        #echo -n '<emphasis>'
        inemphasis=y
      else
        #echo -n '</emphasis>'
        echo -n $defsol
        inemphasis=''
      fi
      continue
    fi

    # `code`

    if [[ "${buf:$i:1}" = '`' ]]; then
      if [[ -z "$incode" ]]; then
        echo -n "$md_code"
        #echo -n '<code>'
        incode=y
      else
        #echo -n '</code>'
        echo -n $defsol
        incode=''
      fi
      continue
    fi

    # "

    if [[ "${buf:$i:1}" = '"' ]]; then
      if [[ -z "$indquote" ]]; then
        echo -n '“'
        indquote=y
      else
        echo -n '”'
        indquote=''
      fi
      continue
    fi

    # ' 

    if [[ "${buf:$i:1}" = "'" ]]; then
      if [[ -z "$inquote" ]]; then
        echo -n '‘'
        inquote=y
      else
        echo -n '’'
        inquote=''
      fi
      continue
    fi

    # <-

    if [[ "${buf:$i:4}" = ' <- ' ]]; then
      echo -n " ← "
      i=$[i+3]
      continue
    fi

    # ->

    if [[ "${buf:$i:4}" = ' -> ' ]]; then
      echo -n " → "
      i=$[i+3]
      continue
    fi

    # ...
    
    if [[ "${buf:$i:3}" = '...' ]]; then
      echo -n "…"
      i=$[i+2]
      continue
    fi

    # <link>

    if [[ "${buf:$i:1}" = '<' ]]; then
      if [[ -z "$inlink" ]]; then
        echo -n $md_code${buf:$i:1}
        #echo -n '<link>'
        inlink=y
      fi
      continue
    fi
    if [[ "${buf:$i:1}" = '>' ]]; then
      if [[ -n "$inlink" ]]; then
        #echo -n '</link>'
        echo -n ${buf:$i:1}$defsol
        inlink=''
      fi
      continue
    fi

    echo -n "${buf:$i:1}"
  done
  echo -n $reset
} && export -f mark

open () {
  # TODO improve the mime handling
  case $PLATFORM in
    mac) open $* ;;
    windows) telln 'Not yet supported.';;
    linux) xdg-open $*;;
  esac
} && export -f open

confirm () {
  declare yn
  read -p " [y/N] " yn
  [[ ${yn,,} =~ y(es)? ]] && return 0
  return 1
} && export -f confirm

tell () {
  declare buf=$(argsorin "$*")
  mark $md_plain "$buf"
} && export -f tell

telln () {
  tell "$*"; echo
} && export -f telln

remind () {
  declare buf=$(argsorin "$*")
  tell "*REMEMBER:* $buf"
} && export -f remind

remindln () {
  remind "$*"; echo
} && export -f remindln

danger () {
  declare buf=$(argsorin "$*")
  tell "${blinkon}***DANGER:***${blinkoff} $buf"
} && export -f danger

dangerln () {
  danger "$*"; echo
} && export -f dangerln

warn () {
  declare buf=$(argsorin "$*")
  tell "${yellow}WARNING:${reset} $buf"
} && export -f warn

warnln () {
  warn "$*"; echo
} && export -f warnln

usageln () {
  declare cmd="$1"; shift
  declare buf="$*"
  tell "**usage:** *$cmd* $buf"
  echo
} && export -f usageln
 
# When we must.

alias grep='grep -i --colour=always'
alias egrep='egrep -i --colour=always'
alias fgrep='fgrep -i --colour=always'

alias diff='diff --color=always'

# TODO maybe later
#diff () {
  #diff --color=always $* | less
#} && export -f diff

# Recursively greps everything (except the stuff with -name). The first
# /dev/null is an old trick to force find to add the name/path to the
# results.
# TODO rewrite with kat and perg instead of the subshells
grepall () {
  find .                          \
    -name '.git'                  \
    -prune -o                     \
    -exec grep -i --color=always "$1" {} \
    /dev/null                     \
    2>/dev/null \;
} && export -f grepall

# Simple egrep-ish thing without the subshell and with Bash regular
# expressions.

perg () {
  declare exp="$1"; shift
  if [[ -z "$*" ]]; then
    while IFS= read -r line; do
      [[ $line =~ $exp ]] && echo "$line"
    done
    return
  fi
  for file in $*; do
    while IFS= read -r line; do
      [[ $line =~ $exp ]] && echo "$line"
    done < "$file"
  done
} && export -f perg

lower () {
  echo ${1,,}
} && export -f lower

upper () {
  echo ${1^^}
} && export -f upper

linecount () {
   IFS=$'\n'
   declare a=($1)
   echo ${#a[@]}
} && export -f linecount

# These are so much faster than basename and dirname.

basepart () {
  echo ${1##*/}
} && export -f basepart

dirpart () {
  echo ${1%/*}
} && export -f dirpart

# Allows the reading of all combined arguments into a string buffer that
# is then echoed, or if no arguments are detected reads fully from
# standard input until there is none left and echoes that. This is notable
# because it allows the user of heredoc input instead of arguments making
# for much cleaner blocks of text See htitle, tell (which is used in the
# and the setup script including in this repo) for how to use it.

argsorin () {
  declare buf="$*"
  [[ -n "$buf" ]] && echo -n "$buf" && return
  while IFS= read -r line; do 
    buf=$buf$line$'\n'
  done
  echo "$buf"
} && export -f argsorin

# A friendlier cat that does not invoke a subshell.

kat () {
	declare line
  if [[ -z "$*" ]]; then
    while IFS= read -r line; do
      [[ $line =~ $exp ]] && echo "$line"
    done
    return
  fi
  for file in $*; do
    while IFS= read -r line; do
      echo "$line"
    done < "$file"
  done
} && export -f kat

# Same as kat but skips blank and lines with optional whitespace that
# begin with a comment character (#).

katlines () {
  for file in $*; do
    while IFS= read -r line; do
      [[ $line =~ ^\ *(#|$) ]] && continue
      echo "$line"
    done < "$file"
  done
} && export -f katlines


# True if anything that can be run from the command line exists, binaries,
# scripts, aliases, and functions.

havecmd () {
  type "$1" &>/dev/null
  return $?
} && export -f havecmd

# Moves an existing thing to the same path and name but with
# ".preserved.<tstamp>" at the end and echoes the new location. Usually
# preferable to destroying what was previously there. Can be used to roll
# back changes transactionally.

preserve () {
  declare target="${1%/}"
  [[ ! -e "$target" ]] && return 1
  declare new="$target.preserved.$(tstamp)"
  mv "$target" "$new" 
  echo "$new" 
} && export -f preserve

# Lists all the preserved files by matching the .preserved<tstamp> suffix.
# If passed an argument limits to only those preserved files matching that
# name (prefix).

lspreserved () {
  declare -a files
  if [[ -z "$1" ]]; then
      files=(*.preserved.[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])
  else
      files=("$1".preserved.[2-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])
  fi
  for i in "${files[@]}"; do
    echo "$i"
  done
} && export -f lspreserved

lastpreserved () {
  mapfile -t  a < <(lspreserved "$1")
  declare n=${#a[@]}
  echo "${a[n-1]}"
} && export -f lastpreserved

rmpreserved () {
  while IFS= read -r line; do
    rm -rf "$line"
  done < <(lspreserved)
} && export -f rmpreserved

# Undos the last preserve performed on the given target.

unpreserve () {
  declare last=$(lastpreserved "$*")
  [[ -z "$last" ]] && return 1
  mv "$last" "${last%.preserved*}"
} && export -f unpreserve

trash () {
  [[ -z "$TRASH" ]] && return 1
  mkdir -p "$TRASH"
  mv "$*" "$TRASH"
} && export -f trash

symlink () {
  declare from="$1"
  declare to="$2"
  if [[ -z "$from" || -z "$to" ]]; then
    usageln 'symlink <from> <to>'
    return 1
  fi
  preserve "$from"
  telln Linking $from'` -> `'$to 
  ln -fs $to $from                   # always hated that this is backwards
} && export -f symlink

# Changes the suffixes from the first argument to the second argument for
# all files specified after that as arguments. Use globstar expansion to
# recurse.

chsuffix () {
  declare i
  declare from="$1"; shift
  declare to="$1"; shift
  declare files=("$@")
  if [[ -z "$files" ]]; then
    usageln 'chsuffix <from> <to> <file> ...'
    return 1
  fi
  for i in "${files[@]}";do
    if [[ "$i" =~ $from$ ]]; then
      declare stripped=${i%$from}
      telln "Moving $i"'` -> `'"$stripped$to"
      mv "$i" "$stripped$to"
    fi
  done
} && export -f chsuffix

anotherterminal () {
  case "$PLATFORM" in 
    mac) ;;
    linux) gnome-terminal;;
  esac
} && export -f anotherterminal

stringscan () {
  declare buf="$1"
  declare handle=(echo)
  [[ -n "$2" ]] && handle=($2)
  for (( i=0; i<${#buf}; i++ )); do 
    ($handle ${buf:$i:1})
  done
} && export -f stringscan

firstline () {
  declare line
  declare IFS=
  read -r line <"$1"
  echo $line
} && export -f firstline


lastline () {
  # Tail is fastest because it does a seek to the end of file.
  tail -1 "$1"
} && export -f lastline

lastcmdline () {
  lastline "$HOME/.bash_history"
} && export -f lastcmdline

lastcmd () {
  declare cl=$(lastcmdline)
  echo ${cl%% *}
} && export -f lastcmd


############################### Date / Time ##############################

tstamp () {
  echo -n $1
  date '+%Y%m%d%H%M%S'
} && export -f tstamp

tstampfile () {
  declare path="$1"
  declare pre=${path%.*}
  declare suf=${path##*.}
  echo -n $pre.$(tstamp)
  [[  "$pre" != "$suf" ]] && echo .$suf
} && export -f tstampfile

now () {
  echo "$1" $(date "+%A, %B %e, %Y, %l:%M:%S%p")
} && export -f now

now-s () {
  echo "$1" $(date "+%A, %B %e, %Y, %l:%M %p")
} && export -f now-s

epoch () {
  date +%s
} && export -f now

watchnow () {
  declare -i delay="${1:-10}"
  havecmd setterm && setterm --cursor off
  trapterm 'setterm --cursor on; clear'
  while true; do 
    clear
    echo -n $(now-s) |lolcat
    now-s > ~/.now
    sleep $delay
  done
} && export -f watchnow

weekday () {
  echo $(lower $(date +"%A"))
} && export -f weekday

month () {
  echo $(lower $(date +"%B"))
} && export -f month

year () {
  echo $(lower $(date +"%Y"))
} && export -f year

week () {
  date +%W
} && export -f week

# Calls the compact ncal variation with Mondays first and including the
# week count during the year. I prefer keeping track of blogs and such by
# the week of the year and not more complicated months and dates. (No one
# ever plugs in a specific month and day into a long blog URL. They just
# want it to be short).

cal () {
  declare exe=$(which ncal)
  if [[ -z "$exe" ]]; then
    cal $* 
    return $?
  fi
  $exe -M -w $*
} && export -f cal

newest () {
  IFS=$'\n'
  f=($(ls -1 --color=never -trd ${1:-.}/*))
  echo "${f[-1]}"
} && export -f newest

lastdown () { echo "$(newest $DOWNLOADS)"; } && export -f lastdown
lastpic () { echo "$(newest $PICTURES)"; } && export -f lastpic
mvlast () { mv "$(lastdown)" "./$1"; } && export -f mvlast
mvlastpic () { mv "$(lastpic)" "./$1"; } && export -f mvlastpic

ex () {
  declare file=$1
  [[ -z "$file" ]] && usageln 'ex <compressed>' && return 1
  [[ ! -f "$file" ]] && tell 'Invalid file: `'$file'`' && return 1
  case $file in
    *.tar.bz2)   tar xjf $file;;
    *.tar.gz)    tar xzf $file;;
    *.bz2)       bunzip2 $file;;
    *.rar)       unrar x $file;;
    *.gz)        gunzip $file;;
    *.tar)       tar xf $file;;
    *.tbz2)      tar xjf $file;;
    *.tgz)       tar xzf $file;;
    *.zip)       unzip $file;;
    *.Z)         uncompress $file;;
    *.7z)        7z x $file;;
    *.xz)        unxz $file;;
    *)           tell 'Unknown suffix on file: `'$file'`'; return 1 ;;
  esac
}

eject () {
  declare path=/media/$USER
  declare first=$(ls -1 $path | head -1)
  declare mpoint=$path/$first
  [[ -z "$first" ]] && tell 'Nothing to eject.' && return
  umount $mpoint && tell "$first ejected" || tell 'Could not eject.'
} && export -f eject

usb () {
  declare path="/media/$USER"
  declare first=$(ls -1 "$path" | head -1)
  echo "$path/$first"
} && export -f usb

cdusb () {
  cd "$(usb)"
} && export -f cdusb

zeroblk () {
  [[ -z "$1" ]] && usageln 'zerobkd <blkdev>' && return 1
  [[ ! -b "/dev/$1" ]] && tell 'Not a block device.' && return 1
  danger 'Are you really sure you want to completly erase /dev/$1?'\
    || return 1
  sudo dd if=/dev/zero of=/dev/$1 bs=4M status=progress
  sync
} && export -f zeroblk


alias bat='\
  upower -i /org/freedesktop/UPower/devices/battery_BAT0 | \
  grep -E "state|to\ full|percentage"'
alias free='free -h'
alias df='df -h'
alias syserrors="sudo journalctl -p 3 -xb"
alias sysderrors="sudo systemctl --failed"
#alias top="gotop -c solarized -f -r .33 -m"
alias top="gotop -r .33"

listening () {
  case "$PLATFORM" in
    mac)                                 # TODO rewrite with perg 
      netstat -an -ptcp | grep LISTEN
      lsof -i -P | grep -i "listen"
      ;;
    *) netstat -tulpn |grep LISTEN ;;
  esac
} && export -f listening

sudolistening () {
  case "$PLATFORM" in
    mac)                                 # TODO rewrite with perg 
      sudo netstat -an -ptcp | grep LISTEN
      sudo lsof -i -P | grep -i "listen"
      ;;
    *) sudo netstat -tulpn |grep LISTEN ;;
  esac
} && export -f sudolistening

sshspeedtest () {
  yes | pv |ssh "$1" "cat >/dev/null"
} && export -f sshspeedtest

if havecmd dircolors; then
  if [ -r ~/.dircolors ]; then
    eval "$(dircolors -b ~/.dircolors)"
  else
    eval "$(dircolors -b)"
  fi
fi

alias lsplain='ls -h --color=never'
alias ls='ls -ah --color=auto'
alias lx='ls -AlXB'    #  Sort by extension.
alias lxr='ls -ARlXB'  #  Sort by extension.
alias lk='ls -AlSr'    #  Sort by size, biggest last.
alias lkr='ls -ARlSr'  #  Sort by size, biggest last.
alias lt='ls -Altr'    #  Sort by date, most recent last.
alias ltr='ls -ARltr'  #  Sort by date, most recent last.
alias lc='ls -Altcr'   #  Sort by change time, most recent last.
alias lcr='ls -ARltcr' #  Sort by change time, most recent last.
alias lu='ls -Altur'   #  Sort by access time, most recent last.
alias lur='ls -ARltur' #  Sort by access time, most recent last.
alias ll='ls -Alhv'    #  A better long listing.
alias llr='ls -ARlhv'  #  Recursive long listing.
alias lr='ll -AR'      #  Recursive simple ls.
alias lm='ls |more'    #  Pipe through 'more'
alias lmr='lr |more'   #  Pipe through 'more'


################################## Curl ##################################

alias ipinfo="curl ipinfo.io"
alias weather="curl wttr.in"

# Igor Chubin is a god.

cheat() {
  where="$1"; shift
  IFS=+ curl "http://cht.sh/$where/ $*"
} && export -f cheat

############################ Secure Shell (SSH) ##########################

# At this point elliptical curve (25519) is the only algorithm anyone
# should ever use. If you haven't upgraded you really should.

sshkeygen () {
  yes '' | ssh-keygen -t ed25519
} && export -f sshkeygen

sshpubkey () {
  declare name=id_ed25519
  [[ -n "$1" ]] && name="$1"
  cat $HOME/.ssh/$name.pub
} && export -f sshpubkey

sshhosts () {
  declare file="$HOME/.ssh/config"
  [[ -f $file ]] || return 1
  while read -r line; do
    [[ "$line" =~ ^Host\ *([^\ ]*) ]] || continue
    echo ${BASH_REMATCH[1]}
  done < "$file"
} && export -f sshhosts && complete -W ssh "$(sshhosts)"


############################ Web API / Tokens ############################

token () {
  [[ -z "$TOKENS" ]] && telln '`$TOKENS` is not set.' && return 1
  declare file="$TOKENS/$1"
  [[ ! -r "$file" ]] && return 1
  firstline "$TOKENS/$1"
} && export -f token

trapterm () {
  declare handler="$1"
  # TODO replace this with a pop/push alternative to preserve keep others
  trap "$handler; echo $'\b\b'; trap -- - SIGINT SIGTERM" SIGTERM SIGINT
} && export -f trapterm

############################### Completion ###############################

# Most completion is set near the function that uses it or internally inside
# the command itself using https://github.com/robmuh/cmdtab for completion.

if [[ -r /usr/share/bash-completion/bash_completion ]]; then
  . /usr/share/bash-completion/bash_completion
fi

if [ $PLATFORM == mac ]; then
  if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
  fi
fi

########################## Terminal ANSI Escapes #########################

alias fixstty='stty sane'

export escape=$'\033'
export reset=$'\033[0m'
export bold=$'\033[1m'
export underline=$'\033[4m'
export blinkon=$'\033[5m'
export blinkoff=$'\033[25m'
export inverse=$'\033[7m'
export inverseoff=$'\033[27m'
export normal=$'\033[39m'
export normalbg=$'\033[49m'

export clear=$'\033[H\033[2J'
clear () { echo -n $clear; } && export -f clear

# TODO lookup clearline (I have someplace in Go)

############################### ANSI Colors ##############################

ansicolors () {
for attr in 0 1 4 5 7 ; do
    echo "------------------------------------------------"
    printf "ESC[%s;Foreground;Background - \n" $attr
    for fore in 30 31 32 33 34 35 36 37; do
        for back in 40 41 42 43 44 45 46 47; do
            printf '\033[%s;%s;%sm %02s;%02s\033[0m' \
              $attr $fore $back $fore $back
        done
    printf '\n'
    done
done
} && export -f ansicolors

# foregrounds

_x=30; export black=$'\033['$_x'm'   # black
_r=31; export red=$'\033['$_r'm'     # red
_g=32; export green=$'\033['$_g'm'   # green
_y=33; export yellow=$'\033['$_y'm'  # yellow
_b=34; export blue=$'\033['$_b'm'    # blue
_m=35; export magenta=$'\033['$_m'm' # magenta
_c=36; export cyan=$'\033['$_c'm'    # cyan
_w=37; export white=$'\033['$_w'm'   # white

# backgrounds

_xb=40; export bgblack=$'\033['$_xb'm'   # bgblack
_rb=41; export bgred=$'\033['$_rb'm'     # bgred
_gb=42; export bggreen=$'\033['$_gb'm'   # bggreen
_yb=43; export bgyellow=$'\033['$_yb'm'  # bgyellow
_bb=44; export bgblue=$'\033['$_bb'm'    # bgblue
_mb=45; export bgmagenta=$'\033['$_mb'm' # bgmagenta
_cb=46; export bgcyan=$'\033['$_cb'm'    # bgcyan
_wb=47; export bgwhite=$'\033['$_wb'm'   # bgwhite

# modern bright (not bold) foregrounds

_X=90; export brblack=$'\033['$_X'm'   # brblack
_R=91; export brred=$'\033['$_R'm'     # brred
_G=92; export brgreen=$'\033['$_G'm'   # brgreen
_Y=93; export bryellow=$'\033['$_Y'm'  # bryellow
_B=94; export brblue=$'\033['$_B'm'    # brblue
_M=95; export brmagenta=$'\033['$_M'm' # brmagenta
_C=96; export brcyan=$'\033['$_C'm'    # brcyan
_W=97; export brwhite=$'\033['$_W'm'   # brwhite

# modern bright (not bold) backgrounds

_Xb=100 # brblack
_Rb=101 # brred
_Gb=102 # brgreen
_Yb=103 # bryellow
_Bb=104 # brblue
_Mb=105 # brmagenta
_Cb=106 # brcyan
_Wb=107 # brwhite

############################ Solarized Colors ############################

# https://ethanschoonover.com/solarized/

# Modern terminals no longer require Solarized to use bold (\033[1m) and
# can use the equivalent background numbers instead (40+). Unfortunately
# for backward compatibility this means that bold itself cannot be used
# dependably with Solarized because it also switches to the legacy color
# variation (for example, bold-red becomes bold-orange instead).
# (Personally I've abandonned Solarized for a more traditional approach
# with higher contrast -- particularly for visibility on small devices
# where I'm told people watch my stream (when I'm not playing around with
# retro terminals for fun.)

# (Yes I know about 256 and rgb color escapes, not interested, but if you
# *are* interested and want to use the entire CSS specification for every
# color ever named have a look at https://github.com/rwxrob/go-colors to
# get all the specific RGB values.)

export soly=$'\033['$_y'm'; export yellow=$soly
export solo=$'\033['$_R'm'; export orange=$solo
export solr=$'\033['$_r'm'; export red=$solr
export solm=$'\033['$_m'm'; export magen=$solm
export solv=$'\033['$_M'm'; export violet=$solv
export solb=$'\033['$_b'm'; export blue=$solb
export solc=$'\033['$_c'm'; export cyan=$solc
export solg=$'\033['$_g'm'; export green=$solg

export solb03=$'\033['$_X'm'; export base03=$solb03
export solb02=$'\033['$_x'm'; export base02=$solb02
export solb01=$'\033['$_G'm'; export base01=$solb01
export solb00=$'\033['$_Y'm'; export base00=$solb00
export solb0=$'\033['$_B'm';  export base0=$solb0
export solb1=$'\033['$_C'm';  export base1=$solb1
export solb2=$'\033['$_w'm';  export base2=$solb2
export solb3=$'\033['$_W'm';  export base3=$solb3
 
export solY=$'\033['$_yb'm'; export bgyellow=$solY
export solO=$'\033['$_Rb'm'; export bgorange=$solO
export solR=$'\033['$_rb'm'; export bgred=$solR
export solM=$'\033['$_mb'm'; export bgmagen=$solM
export solV=$'\033['$_Mb'm'; export bgviolet=$solV
export solB=$'\033['$_bb'm'; export bgblue=$solB
export solC=$'\033['$_cb'm'; export bgcyan=$solC
export solG=$'\033['$_gb'm'; export bggreen=$solG

export solB03=$'\033['$_Xb'm'; export bgbase03=$solB03
export solB02=$'\033['$_xb'm'; export bgbase02=$solB02
export solB01=$'\033['$_Gb'm'; export bgbase01=$solB01
export solB00=$'\033['$_Yb'm'; export bgbase00=$solB00
export solB0=$'\033['$_Bb'm';  export bgbase0=$solB0
export solB1=$'\033['$_Cb'm';  export bgbase1=$solB1
export solB2=$'\033['$_wb'm';  export bgbase2=$solB2
export solB3=$'\033['$_Wb'm';  export bgbase3=$solB3

solcolors=(y o r m v b c g)
solbases=(b03 b02 b01 b00 b0 b1 b2 b3)

# TODO add a decolor (port from Go version)

rnd () {
  declare num=$[RANDOM%8]
  declare col=${solcolors[$num]}
  declare name=sol$col
  echo -n ${!name}
} && export -f rnd

iscolor () {
  case "$1" in
    $yellow|$orange|$red|$magen|$violet|$blue|$cyan|$green)
      return 0
      ;;
  esac
  return 1
} && export -f iscolor

isbgcolor () {
  case "$1" in
    $bgyellow|$bgorange|$bgred|$bgmagen|$bgviolet|$bgblue|$bgcyan|$bggreen)
      return 0
      ;;
  esac
  return 1
} && export -f isbgcolor

isbase () {
  case "$1" in
    $base03|$base02|$base01|$base00|$base0|$base1|$base2|$base3)
      return 0
      ;;
  esac
  return 1
} && export -f isbase

isbgbase () {
  case "$1" in
    $bgbase03|$bgbase02|$bgbase01|$bgbase00|$bgbase0|$bgbase1|$bgbase2|$bgbase3)
      return 0
      ;;
  esac
  return 1
} && export -f isbgbase

issol () {
  iscolor $1 && return 0
  isbase $1 && return 0
  isbgcolor $1 && return 0
  isbgbase $1 && return 0
  return 1
} && export -f issol

# ooooooo, pretty

sols () {
  printf "     "
  for fore in ${solbases[@]} ${solcolors[@]} ;do
    declare c=sol$fore
    printf "%-4s" $fore
  done
  printf $reset
  for back in ${solbases[@]} ${solcolors[@]};do
    declare b=sol${back^^}
    printf "$reset\n %-4s%s" ${back^^} ${!b} 
    for fore in ${solbases[@]} ${solcolors[@]};do
      declare f=sol${fore}
      printf "%s%s%-4s" ${!b} ${!f} sol
    done
  done
  printf $reset
  printf "\n[reset clear]\n"
} && export -f sols

############################### Modern RGB ###############################

rgb () {
  echo -n $'\033[38;2;'$1';'$2';'$3'm'
} && export -f rgb

rgbg () {
  echo -n $'\033[48;2;'$1';'$2';'$3'm'
} && export -f rgb

############################## Named Colors ##############################

export twitch=$'\033[38;2;169;112;255m'
export twrwxrob=$'\033[38;2;230;77;73m'

########################### CSS Standard Colors ##########################

# Thanks to the wonderful work of Ethan Baker (github.com/ethanbaker).

export aliceblue=$'\033[38;2;240;248;255m'
export antiquewhite=$'\033[38;2;250;235;215m'
export aqua=$'\033[38;2;0;255;255m'
export aquamarine=$'\033[38;2;127;255;212m'
export azure=$'\033[38;2;1240;255;255m'
export beige=$'\033[38;2;245;245;220m'
export bisque=$'\033[38;2;255;228;196m'
export black=$'\033[38;2;0;0;0m'
export blanchedalmond=$'\033[38;2;255;235;205m'
export blue=$'\033[38;2;0;0;255m'
export blueviolet=$'\033[38;2;138;43;226m'
export brown=$'\033[38;2;165;42;42m'
export burlywood=$'\033[38;2;222;184;135m'
export cadetblue=$'\033[38;2;95;158;160m'
export chartreuse=$'\033[38;2;95;158;160m'
export chocolate=$'\033[38;2;210;105;30m'
export coral=$'\033[38;2;255;127;80m'
export cornflowerblue=$'\033[38;2;100;149;237m'
export cornsilk=$'\033[38;2;255;248;220m'
export crimson=$'\033[38;2;220;20;60m'
export cyan=$'\033[38;2;0;255;255m'
export darkblue=$'\033[38;2;0;0;139m'
export darkcyan=$'\033[38;2;0;139;139m'
export darkgoldenrod=$'\033[38;2;184;134;11m'
export darkgray=$'\033[38;2;169;169;169m'
export darkgreen=$'\033[38;2;0;100;0m'
export darkkhaki=$'\033[38;2;189;183;107m'
export darkmagenta=$'\033[38;2;139;0;139m'
export darkolivegreen=$'\033[38;2;85;107;47m'
export darkorange=$'\033[38;2;255;140;0m'
export darkorchid=$'\033[38;2;153;50;204m'
export darkred=$'\033[38;2;139;0;0m'
export darksalmon=$'\033[38;2;233;150;122m'
export darkseagreen=$'\033[38;2;143;188;143m'
export darkslateblue=$'\033[38;2;72;61;139m'
export darkslategray=$'\033[38;2;47;79;79m'
export darkturquoise=$'\033[38;2;0;206;209m'
export darkviolet=$'\033[38;2;148;0;211m'
export deeppink=$'\033[38;2;255;20;147m'
export deepskyblue=$'\033[38;2;0;191;255m'
export dimgray=$'\033[38;2;0;191;255m'
export dodgerblue=$'\033[38;2;30;144;255m'
export firebrick=$'\033[38;2;178;34;34m'
export floralwhite=$'\033[38;2;255;250;240m'
export forestgreen=$'\033[38;2;34;139;34m'
export fuchsia=$'\033[38;2;255;0;255m'
export gainsboro=$'\033[38;2;220;220;220m'
export ghostwhite=$'\033[38;2;248;248;255m'
export gold=$'\033[38;2;255;215;0m'
export goldenrod=$'\033[38;2;218;165;32m'
export gray=$'\033[38;2;127;127;127m'
export green=$'\033[38;2;0;128;0m'
export greenyellow=$'\033[38;2;173;255;47m'
export honeydew=$'\033[38;2;240;255;240m'
export hotpink=$'\033[38;2;255;105;180m'
export indianred=$'\033[38;2;205;92;92m'
export indigo=$'\033[38;2;75;0;130m'
export ivory=$'\033[38;2;255;255;240m'
export khaki=$'\033[38;2;240;230;140m'
export lavender=$'\033[38;2;230;230;250m'
export lavenderblush=$'\033[38;2;255;240;245m'
export lawngreen=$'\033[38;2;124;252;0m'
export lemonchiffon=$'\033[38;2;255;250;205m'
export lightblue=$'\033[38;2;173;216;230m'
export lightcoral=$'\033[38;2;240;128;128m'
export lightcyan=$'\033[38;2;224;255;255m'
export lightgoldenrodyellow=$'\033[38;2;250;250;210m'
export lightgreen=$'\033[38;2;144;238;144m'
export lightgrey=$'\033[38;2;211;211;211m'
export lightpink=$'\033[38;2;255;182;193m'
export lightsalmon=$'\033[38;2;255;160;122m'
export lightseagreen=$'\033[38;2;32;178;170m'
export lightskyblue=$'\033[38;2;135;206;250m'
export lightslategray=$'\033[38;2;119;136;153m'
export lightsteelblue=$'\033[38;2;176;196;222m'
export lightyellow=$'\033[38;2;255;255;224m'
export lime=$'\033[38;2;0;255;0m'
export limegreen=$'\033[38;2;50;205;50m'
export linen=$'\033[38;2;250;240;230m'
export magenta=$'\033[38;2;255;0;255m'
export maroon=$'\033[38;2;128;0;0m'
export mediumaquamarine=$'\033[38;2;102;205;170m'
export mediumblue=$'\033[38;2;0;0;205m'
export mediumorchid=$'\033[38;2;186;85;211m'
export mediumpurple=$'\033[38;2;147;112;219m'
export mediumseagreen=$'\033[38;2;60;179;113m'
export mediumslateblue=$'\033[38;2;123;104;238m'
export mediumspringgreen=$'\033[38;2;0;250;154m'
export mediumturquoise=$'\033[38;2;72;209;204m'
export mediumvioletred=$'\033[38;2;199;21;133m'
export midnightblue=$'\033[38;2;25;25;112m'
export mintcream=$'\033[38;2;245;255;250m'
export mistyrose=$'\033[38;2;255;228;225m'
export moccasin=$'\033[38;2;255;228;181m'
export navajowhite=$'\033[38;2;255;222;173m'
export navy=$'\033[38;2;0;0;128m'
export navyblue=$'\033[38;2;159;175;223m'
export oldlace=$'\033[38;2;253;245;230m'
export olive=$'\033[38;2;128;128;0m'
export olivedrab=$'\033[38;2;107;142;35m'
export orange=$'\033[38;2;255;165;0m'
export orangered=$'\033[38;2;255;69;0m'
export orchid=$'\033[38;2;218;112;214m'
export palegoldenrod=$'\033[38;2;238;232;170m'
export palegreen=$'\033[38;2;152;251;152m'
export paleturquoise=$'\033[38;2;175;238;238m'
export palevioletred=$'\033[38;2;219;112;147m'
export papayawhip=$'\033[38;2;255;239;213m'
export peachpuff=$'\033[38;2;255;218;185m'
export peru=$'\033[38;2;205;133;63m'
export pink=$'\033[38;2;255;192;203m'
export plum=$'\033[38;2;221;160;221m'
export powderblue=$'\033[38;2;176;224;230m'
export purple=$'\033[38;2;128;0;128m'
export red=$'\033[38;2;255;0;0m'
export rosybrown=$'\033[38;2;188;143;143m'
export royalblue=$'\033[38;2;65;105;225m'
export saddlebrown=$'\033[38;2;139;69;19m'
export salmon=$'\033[38;2;250;128;114m'
export sandybrown=$'\033[38;2;244;164;96m'
export seagreen=$'\033[38;2;46;139;87m'
export seashell=$'\033[38;2;255;245;238m'
export sienna=$'\033[38;2;160;82;45m'
export silver=$'\033[38;2;192;192;192m'
export skyblue=$'\033[38;2;135;206;235m'
export slateblue=$'\033[38;2;106;90;205m'
export slategray=$'\033[38;2;112;128;144m'
export snow=$'\033[38;2;255;250;250m'
export springgreen=$'\033[38;2;0;255;127m'
export steelblue=$'\033[38;2;70;130;180m'
export tan=$'\033[38;2;210;180;140m'
export teal=$'\033[38;2;0;128;128m'
export thistle=$'\033[38;2;216;191;216m'
export tomato=$'\033[38;2;255;99;71m'
export turquoise=$'\033[38;2;64;224;208m'
export violet=$'\033[38;2;238;130;238m'
export wheat=$'\033[38;2;245;222;179m'
export white=$'\033[38;2;255;255;255m'
export whitesmoke=$'\033[38;2;245;245;245m'
export yellow=$'\033[38;2;255;255;0m'
export yellowgreen=$'\033[38;2;139;205;50m'

export alicebluebg=$'\033[48;2;240;248;255m'
export antiquewhitebg=$'\033[48;2;250;235;215m'
export aquabg=$'\033[48;2;0;255;255m'
export aquamarinebg=$'\033[48;2;127;255;212m'
export azurebg=$'\033[48;2;1240;255;255m'
export beigebg=$'\033[48;2;245;245;220m'
export bisquebg=$'\033[48;2;255;228;196m'
export blackbg=$'\033[48;2;0;0;0m'
export blanchedalmondbg=$'\033[48;2;255;235;205m'
export bluebg=$'\033[48;2;0;0;255m'
export bluevioletbg=$'\033[48;2;138;43;226m'
export brownbg=$'\033[48;2;165;42;42m'
export burlywoodbg=$'\033[48;2;222;184;135m'
export cadetbluebg=$'\033[48;2;95;158;160m'
export chartreusebg=$'\033[48;2;95;158;160m'
export chocolatebg=$'\033[48;2;210;105;30m'
export coralbg=$'\033[48;2;255;127;80m'
export cornflowerbluebg=$'\033[48;2;100;149;237m'
export cornsilkbg=$'\033[48;2;255;248;220m'
export crimsonbg=$'\033[48;2;220;20;60m'
export cyanbg=$'\033[48;2;0;255;255m'
export darkbluebg=$'\033[48;2;0;0;139m'
export darkcyanbg=$'\033[48;2;0;139;139m'
export darkgoldenrodbg=$'\033[48;2;184;134;11m'
export darkgraybg=$'\033[48;2;169;169;169m'
export darkgreenbg=$'\033[48;2;0;100;0m'
export darkkhakibg=$'\033[48;2;189;183;107m'
export darkmagentabg=$'\033[48;2;139;0;139m'
export darkolivegreenbg=$'\033[48;2;85;107;47m'
export darkorangebg=$'\033[48;2;255;140;0m'
export darkorchidbg=$'\033[48;2;153;50;204m'
export darkredbg=$'\033[48;2;139;0;0m'
export darksalmonbg=$'\033[48;2;233;150;122m'
export darkseagreenbg=$'\033[48;2;143;188;143m'
export darkslatebluebg=$'\033[48;2;72;61;139m'
export darkslategraybg=$'\033[48;2;47;79;79m'
export darkturquoisebg=$'\033[48;2;0;206;209m'
export darkvioletbg=$'\033[48;2;148;0;211m'
export deeppinkbg=$'\033[48;2;255;20;147m'
export deepskybluebg=$'\033[48;2;0;191;255m'
export dimgraybg=$'\033[48;2;0;191;255m'
export dodgerbluebg=$'\033[48;2;30;144;255m'
export firebrickbg=$'\033[48;2;178;34;34m'
export floralwhitebg=$'\033[48;2;255;250;240m'
export forestgreenbg=$'\033[48;2;34;139;34m'
export fuchsiabg=$'\033[48;2;255;0;255m'
export gainsborobg=$'\033[48;2;220;220;220m'
export ghostwhitebg=$'\033[48;2;248;248;255m'
export goldbg=$'\033[48;2;255;215;0m'
export goldenrodbg=$'\033[48;2;218;165;32m'
export graybg=$'\033[48;2;127;127;127m'
export greenbg=$'\033[48;2;0;128;0m'
export greenyellowbg=$'\033[48;2;173;255;47m'
export honeydewbg=$'\033[48;2;240;255;240m'
export hotpinkbg=$'\033[48;2;255;105;180m'
export indianredbg=$'\033[48;2;205;92;92m'
export indigobg=$'\033[48;2;75;0;130m'
export ivorybg=$'\033[48;2;255;255;240m'
export khakibg=$'\033[48;2;240;230;140m'
export lavenderbg=$'\033[48;2;230;230;250m'
export lavenderblushbg=$'\033[48;2;255;240;245m'
export lawngreenbg=$'\033[48;2;124;252;0m'
export lemonchiffonbg=$'\033[48;2;255;250;205m'
export lightbluebg=$'\033[48;2;173;216;230m'
export lightcoralbg=$'\033[48;2;240;128;128m'
export lightcyanbg=$'\033[48;2;224;255;255m'
export lightgoldenrodyellowbg=$'\033[48;2;250;250;210m'
export lightgreenbg=$'\033[48;2;144;238;144m'
export lightgreybg=$'\033[48;2;211;211;211m'
export lightpinkbg=$'\033[48;2;255;182;193m'
export lightsalmonbg=$'\033[48;2;255;160;122m'
export lightseagreenbg=$'\033[48;2;32;178;170m'
export lightskybluebg=$'\033[48;2;135;206;250m'
export lightslategraybg=$'\033[48;2;119;136;153m'
export lightsteelbluebg=$'\033[48;2;176;196;222m'
export lightyellowbg=$'\033[48;2;255;255;224m'
export limebg=$'\033[48;2;0;255;0m'
export limegreenbg=$'\033[48;2;50;205;50m'
export linenbg=$'\033[48;2;250;240;230m'
export magentabg=$'\033[48;2;255;0;255m'
export maroonbg=$'\033[48;2;128;0;0m'
export mediumaquamarinebg=$'\033[48;2;102;205;170m'
export mediumbluebg=$'\033[48;2;0;0;205m'
export mediumorchidbg=$'\033[48;2;186;85;211m'
export mediumpurplebg=$'\033[48;2;147;112;219m'
export mediumseagreenbg=$'\033[48;2;60;179;113m'
export mediumslatebluebg=$'\033[48;2;123;104;238m'
export mediumspringgreenbg=$'\033[48;2;0;250;154m'
export mediumturquoisebg=$'\033[48;2;72;209;204m'
export mediumvioletredbg=$'\033[48;2;199;21;133m'
export midnightbluebg=$'\033[48;2;25;25;112m'
export mintcreambg=$'\033[48;2;245;255;250m'
export mistyrosebg=$'\033[48;2;255;228;225m'
export moccasinbg=$'\033[48;2;255;228;181m'
export navajowhitebg=$'\033[48;2;255;222;173m'
export navybg=$'\033[48;2;0;0;128m'
export navybluebg=$'\033[48;2;159;175;223m'
export oldlacebg=$'\033[48;2;253;245;230m'
export olivebg=$'\033[48;2;128;128;0m'
export olivedrabbg=$'\033[48;2;107;142;35m'
export orangebg=$'\033[48;2;255;165;0m'
export orangeredbg=$'\033[48;2;255;69;0m'
export orchidbg=$'\033[48;2;218;112;214m'
export palegoldenrodbg=$'\033[48;2;238;232;170m'
export palegreenbg=$'\033[48;2;152;251;152m'
export paleturquoisebg=$'\033[48;2;175;238;238m'
export palevioletredbg=$'\033[48;2;219;112;147m'
export papayawhipbg=$'\033[48;2;255;239;213m'
export peachpuffbg=$'\033[48;2;255;218;185m'
export perubg=$'\033[48;2;205;133;63m'
export pinkbg=$'\033[48;2;255;192;203m'
export plumbg=$'\033[48;2;221;160;221m'
export powderbluebg=$'\033[48;2;176;224;230m'
export purplebg=$'\033[48;2;128;0;128m'
export redbg=$'\033[48;2;255;0;0m'
export rosybrownbg=$'\033[48;2;188;143;143m'
export royalbluebg=$'\033[48;2;65;105;225m'
export saddlebrownbg=$'\033[48;2;139;69;19m'
export salmonbg=$'\033[48;2;250;128;114m'
export sandybrownbg=$'\033[48;2;244;164;96m'
export seagreenbg=$'\033[48;2;46;139;87m'
export seashellbg=$'\033[48;2;255;245;238m'
export siennabg=$'\033[48;2;160;82;45m'
export silverbg=$'\033[48;2;192;192;192m'
export skybluebg=$'\033[48;2;135;206;235m'
export slatebluebg=$'\033[48;2;106;90;205m'
export slategraybg=$'\033[48;2;112;128;144m'
export snowbg=$'\033[48;2;255;250;250m'
export springgreenbg=$'\033[48;2;0;255;127m'
export steelbluebg=$'\033[48;2;70;130;180m'
export tanbg=$'\033[48;2;210;180;140m'
export tealbg=$'\033[48;2;0;128;128m'
export thistlebg=$'\033[48;2;216;191;216m'
export tomatobg=$'\033[48;2;255;99;71m'
export turquoisebg=$'\033[48;2;64;224;208m'
export violetbg=$'\033[48;2;238;130;238m'
export wheatbg=$'\033[48;2;245;222;179m'
export whitebg=$'\033[48;2;255;255;255m'
export whitesmokebg=$'\033[48;2;245;245;245m'
export yellowbg=$'\033[48;2;255;255;0m'
export yellowgreenbg=$'\033[48;2;139;205;50m'

# Not a fan of ridiculously distracting command prompts. Prompt should
# fade into background, not demand focus. The printf magic ensures that
# any output that does not end with a newline will not put a prompt
# immediately after it. Never forget the \[$color\] brackets that escape
# the width of the color escapes so they don't affect line wrapping.

# See https://github.com/ryanoasis/powerline-extra-symbols

export glypharrow=$'\ue0b0'
export glyphflames=$'\ue0c0'
export glyphrounded=$'\ue0b4'
export glyphbits=$'\ue0c6'
export glyphhex=$'\ue0cc'

export promptglyph=$glypharrow
export promptbgr=40
export promptbgg=40
export promptbgb=40
export promptuserc=$twrwxrob
export prompthostc=$twitch
export promptdirc=$bold$base04

# TODO detect current home system and only show home of main system so
# that when used on remote systems the name is displayed.

export PROMPT_COMMAND='
  touch "$HOME/.bash_history"
  promptbg=$(rgbg $promptbgr $promptbgg $promptbgb)
  # FIXME 
  promptglyphc=$(rgb $promptbgr $promptbgg $promptbgb)
  if [[ $HOME == $PWD ]]; then
    export PROMPTDIR="🏡"
  elif [[ $HOME == ${PWD%/*} ]]; then
    export PROMPTDIR="/${PWD##*/}"
  elif [[ / == $PWD ]]; then
    export PROMPTDIR="/"
  elif [[ "" == ${PWD%/*} ]]; then
    export PROMPTDIR=$PWD
  else
    # TODO fixeme with ascii char
    export PROMPTDIR=…/${PWD##*/}
  fi
  if [[ $EUID == 0 ]]; then
    PS1="\[$blinkon$promptbg$red\]\u\[$base03$blinkoff\]@\[\$prompthostc\]\h \[$base03\]\[$promptdirc\]"$PROMPTDIR" \[$reset$promptglyphc\]$promptglyph \[$reset\]"
  else 
    PS1="\[$base03\]\[$promptdirc\]"$PROMPTDIR" \[$reset$twitch\]$promptglyph \[$reset\]"
 fi
'
############################# Editor / Pager #############################

# The first and only visual editor that matters. It's on everything. Stop
# complaining and apologizing for it and learn the fucking thing. Then vi
# all the things and make sure to not fuck up your vi muscle memory with
# arrows and shit you cannot use with an old vi you might be forced to use
# someday. There's really no reason. Learn home row and CTRL-[ for escape.

havecmd vim && alias vi=vim

[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
alias more='less -R'

# Here's your colored man pages right here.

export LESS_TERMCAP_mb=$magen
export LESS_TERMCAP_md=$yellow
export LESS_TERMCAP_me=$reset
export LESS_TERMCAP_se=$reset
export LESS_TERMCAP_so=$blue
export LESS_TERMCAP_ue=$reset
export LESS_TERMCAP_us=$violet


###################### Gnome Terminal Configuration ######################

# Gnome terminal configuration is never easy to save. These are the
# documented ways to save, reset, and restore a gnome-configuration
# setting for an individual user. The commands are easy enough to execute
# so these are included mostly as reminders about how to do it.

gnome-terminal-conf-dump () {
  if ! havecmd dconf ; then
    telln 'Cannot locate: `dconf`'
    return 1
  fi
  declare dest="${1:-/tmp/dconf-terminal-$(tstamp)}"
  declare buf=$(dconf dump /org/gnome/terminal/)
  echo "$buf" >| $dest
  echo $dest
} && export -f gnome-terminal-conf-dump

gnome-terminal-conf-reset () {
  if ! havecmd dconf ; then
    telln 'Cannot locate: `dconf`'
    return 1
  fi
  dconf reset -f /org/gnome/terminal/
} && export -f gnome-terminal-conf-reset

gnome-terminal-conf-load () {
  if ! havecmd dconf ; then
    telln 'Cannot locate: `dconf`'
    return 1
  fi
  if [[ -z "$1" ]]; then
    usageln 'gnome-terminal-conf-load <path>'
    return 1
  fi
  dconf load < "$1"
} && export -f gnome-terminal-conf-load

termtitle () {
  printf "\033]0;$*\007"
} && export -f termtitle

alias tmatrix="tmatrix -s 15 --fade"

hidecursor () {
  if havecmd setterm; then
    setterm --cursor off
    trapterm 'setterm --cursor on; clear'
  fi
} && export -f hidecursor
############################## Notifications #############################

# Notify sends a normal notification using notify-send. Use notify-send
# for more specific notifications.

notify () {
  notify-send "$*"
} && export -f notify

################################# History ################################

# This history configuration assume a reasonably conservative system on
# which saving the session history after the session is fine because
# sessions are very frequently started and stopped due to extensive TMUX
# pane usage. Some may prefer to add a `history -a; history -c; history
# -r` to their PROMPT_COMMAND if they tend to stay in the same session for
# a long time and want the history to be updated for all to see.

shopt -s histappend
HISTCONTROL=ignoreboth
HISTSIZE=5000
HISTFILESIZE=10000

################################ ASCII Art ###############################

doh () {
  echo -n $base3
  kat <<'EOM'

                 _ ,___,-'",-=-.           
       __,-- _ _,-'_)_  (""`'-._\ `.  [31m _____   ____  _    _ _  [0m [97m
    _,'  __ |,' ,-' __)  ,-     /. |  [31m|  __ \ / __ \| |  | | |[0m [97m
  ,'_,--'   |     -'  _)/         `\  [31m| |  | | |  | | |__| | |[0m [97m
,','      ,'       ,-'_,`           : [31m| |  | | |  | |  __  | |[0m [97m
,'     ,-'       ,(,-(              : [31m| |__| | |__| | |  | |_|[0m [97m
     ,'       ,-' ,    _            ; [31m|_____/ \____/|_|  |_(_)[0m [97m
    /        ,-._/`---'            /                                
   /        (____)(----. )       ,'                                 
  /         (      `.__,     /\ /,         
 :           ;-.___         /__\/|         
 |         ,'      `--.      -,\ |         
 :        /            \    .__/           
  \      (__            \    |_            
   \       ,`-, *       /   _|,\           
    \    ,'   `-.     ,'_,-'    \          
   (_\,-'    ,'\")--,'-'       __\         
    \       /  // ,'|      ,--'  `-.       
     `-.    `-/ \'  |   _,'         `.     
        `-._ /      `--'/             \    
          ,'           /               \    
          /            |                \  
 [92m-hrr-[97m ,-'             |                /  
      /                               -'   

EOM
  echo -n $reset
} && export -f doh

humm () {
  echo -n $base3
  kat <<'EOM'
       ,---.                        
     ,.'-.   \                      
    ( ( ,'"""""-.                   
    `,X          `.        [33m  _____                   [0m[97m
    /` `           `._     [33m |  |  |_ _ _____ _____   [0m[97m
   (            ,   ,_\    [33m |     | | |     |     |_ [0m[97m
   |          ,---.,'o `.  [33m |__|__|___|_|_|_|_|_|_|_| [0m[97m
   |         / o   \     )          
    \ ,.    (      .____,           
     \| \    \____,'     \          
   '`'\  \        _,____,'          
   \  ,--      ,-'     \            
     ( C     ,'         \           
      `--'  .'           |          
        |   |            |          
      __|    \        ,-'_          
     / `L     `._  _,'  ' `.        
    /    `--.._  `',.   _\  `       
    `-.       /\  | `. ( ,\  \      
   _/  `-._  /  \ |--'  (     \     
 -'  `-.   `'    \/\`.   `.    )    
        \  [92m-hrr-[97m    \ `.  |    |    

EOM
} && export -f humm

##########################################################################

# Prints some asciiart by key name from the exported asciiart associative
# array. If not found says so on stdout and returns 1.

#ascii () {
#  declare name="${1:-hummm}"
#  if [[ -v $asciiart[$name] ]]; then
#    telln 'Sorry no ascii art found for `'$name'`'
#    return 1
#  fi
#  declare text=${asciiart[$name]}
#  echo -n "${text:1}"
#} && export -f ascii

# TODO I *really* need an asciicenter function that detects and
# automatically centers. It's simple enough, I just need to get it done.
# Could have finished already in the time I spend adding spaces for
# padding to the art.

# Joins two files of text (presumably ascii art). You should add adequate
# spaces to the left file so that they match. (Might calculate that
# someday but this is fine for now.)

asciijoin () {
  declare -a a b
  declare i
  mapfile -t  a < "$1"
  mapfile -t  b < "$2"
  declare -i arows=${#a[@]}
  declare -i brows=${#b[@]}
  declare -i rows
  if [[ $arows > $brows ]]; then # ???
    rows=$brows
  else
    rows=$arows
  fi
  #declare -i awidth
  for (( i=0; i<$rows; i++ )); do 
    declare aline=${a[$i]}
    declare bline=${b[$i]}
    declare -i alen=${#aline}
    #[[ $alen > $awidth ]] && awidth=$alen
    echo "$aline $bline"
  done
  # FIXME second pass with left padding in case image on the left has
  # fewer lines than the one on the right
} && export -f asciijoin

alias cmatrix="cmatrix -b -C red -u 6"
alias fish="asciiquarium" # fuck the fish shell

watchdragon () {
  hidecursor
  while true; do
    clear
    echo 
    fortune -s | cowsay -f dragon |lolcat
    sleep 120
  done
} && export -f watchdragon

########################## System Administration #########################

is-valid-username () {
  [[ "$1" =~ ^[a-z_][a-z0-9_]{0,31}$ ]] && return 0
  return 1
} && export -f is-valid-username

#change-user-name () {
#  declare old="$1"
#  declare new="$2"
#  if [[ -z "$old" || -z "$new" ]]; then
#    usageln 'change-user-name <old> <new>'
#    return 1
#  fi
#  ! is-valid-username "$old" && telln 'Invalid old username: `'$old'`' && return 1
#  ! is-valid-username "$new" && telln 'Invalid new username: `'$new'`' && return 1
#  sudo groupadd $new
#  # TODO rename the directory, test on vm
#  sudo usermod -d /home/$new -m -g $new -l $new $old
#}

preview () {
  browser-sync start \
    --no-notify --no-ui \
    --ignore '**/.*' \
    -sw
} && export -f preview

httphead () {
  curl -I -L "$@"
} && export -f httphead

################################ Encoding ################################

rot13 () {
  declare data=$(argsorin "$*")
  echo "$data" | tr '[A-Za-z]' '[N-ZA-Mn-za-m]'
} && export -f rot13


####################### Git Repos / GitHub / GitLab ######################

# Even though the git user.name can be different between repos and
# providers it is best to find a username that is consistent across
# providers and has the same git user.email. If not, stick with one
# provider or override.

havecmd git && export GITUSER="$(git config --global user.name)"

# Detect if the current directory is within a git repository (no matter
# how deep into subdirectories.

isgit () {
  git rev-parse >/dev/null >/dev/null 2>&1
  return $?
} && export -f isgit

# Print the git user.name for the current location.

gituser () { git config user.name; } && export -f gituser

# Print the git user.email for the current location.

gitemail () { git config user.email; } && export -f gitemail

# Print the git user. for the current location.

#gitgpg () { git config TODO???;} && export -f gitgpg

# Print the git remote.origin.url for the current location.

giturl () { git config remote.origin.url; } && export -f giturl

#alias gituser='git config user.name'
#alias gitemail='git config user.email'
#alias gitgpg='git config user.email'
#alias giturl='git config user.email'

# TODO write the comment doc extractor and Pandoc renderer (later)

repo () {
  [[ -z "$REPOS" ]] && telln 'Need to set `$REPOS` first.`' && return 1

  declare manifest="$REPOS/MANIFEST"
  declare subcmd="$1"; shift
  declare server repo rok user url pdir ppath entity

  #TODO seriously consider doing this stuff for every repo subcommand
  # instead of just a select few (most do already).

  case "$subcmd" in
    create|clone|forcedelete|gobadges|delete|show|\
      private|public|listremoteother|listremote|fork|\
      gobadges) 

      
      if [[ -n "$1" ]]; then
        url="$1"
      elif repo inpath; then
        url=$(repo current)
      else
        warnln "Not in $REPOS. Cannot infer repo information."
        return 1
      fi

    #TODO be okay with partial urls in order to infer the service
    #TODO add a default service to .gitconfig (so repo clone does not need
    # full remote path, repo listremote and others need default
    # service, could even make it so partial matches are thrown
    # into a select)

    # TODO grab the git user for each call instead of GITUSER

      server="${url%%/*}"
      repo="${url#*/}"
      entity="${repo%/*}"
      pdir=$(dirpart "$repo")
      reponame=$(basepart "$repo")
      ppath="$REPOS/$pdir"
      tok=$(token "$server")
      ;;
  esac

  case "$subcmd" in

    # Discover descends through the entire $REPOS tree finding Git repos
    # by them having .git directories. It take longer than I would want to
    # do on the command line so it is normally called only from `repo
    # mkmanifest` to create the manifest for faster matches.

    discover) 
      declare dirs=("$REPOS/"**/.git)
      declare dir
      for dir in "${dirs[@]}"; do
        dir=${dir%/.git}
        dir=${dir#$REPOS/}
        echo $dir
      done
      ;;

    # Just simple list of the repos in the current manifest cache.

    list) 
      if [[ ! -e "$manifest" ]]; then
        telln 'Manifest missing, do `repo mkmanifest` to create.'
        return 1
      fi
      katlines "$manifest"
      ;; 

    # Generates a freshly cached $REPOS/MANIFEST file for faster searching
    # and listing. (The full find descent it not particularly responsive.)

    mkmanifest)
      declare dirs=$(repo discover)
      echo '# Generated by `repo mkmanifest`'$(now) >| "$manifest"
      echo >> "$manifest"
      echo "$dirs" >> "$REPOS/MANIFEST" 
      ;;

    # Paths are just the long version of list.

    paths)
      declare repos=($(repo list))
      declare r
      for r in "${repos[@]}"; do
        echo "$REPOS/$r"
      done
      ;;

    # Returns urinary true is current directory is within the $REPOS
    # directory.

    inpath) [[ "$PWD" =~ "$REPOS" ]] && return 0; return 1 ;; 

    # Returns the inferred full repo URL if currently anywhere within a
    # $REPOS subdirectory.

    current) 
      if repo inpath; then
        declare cur="$PWD"
        while [[ "$cur" != "$REPOS" ]]; do
          if [[ -d "$cur/.git" ]]; then
          declare relative=${cur#$REPOS/}
            echo $relative
            return
          else
            cur=${cur%/*}
          fi
        done
      fi
      return 1
    ;;

    # If no arguments are passed at all just changes into the $REPOS
    # directory.

    "") [[ -n "$REPOS" ]] && cd "$REPOS" ;;

    # Matches the keyword or regular expression typed in from the list of
    # repos in the current MANIFEST (repo list).

    match)
      repo list | perg "$1"
      ;;

    # Creates a new repo at the specified URL. The repo at that location
    # will be created and the new repo will be cloned to the local system
    # into the $REPOS directory. Current directory is then changed into
    # the newly cloned directory. Only GitHub is currently supported
    # (github.com).

    create)
      if [[ -e "$REPOS/$url" ]];then
        telln 'Already created: `'$url'`'
        return 1
      fi
      if [[ "$server" == github.com ]]; then
        declare type=user
        if [[ ! "$repo" =~ ^$GITUSER ]];then
          type=orgs/$entity
        fi
        declare -i status=$(curl -X POST -H "Authorization: token $tok" \
          -d '{"name":"'"$reponame"'","private":true}' \
          -w "%{http_code}" \
          -s -o /dev/null \
          "https://api.github.com/$type/repos")
        if [[ $status != 201 ]]; then
          telln '***Failed with status:*** `'$status'`'
          return 1
        fi
        repo clone "$url"
        return
      fi
      telln 'Unsupported Git service or server: `'$server'`'
      return 1
      ;;

    # Clones the URL into the $REPOS directory and changes into the new
    # directory. Also update the local $REPOS/MANIFEST (`repo mkmanifest`)
    # to `match` and `list` return correct results.

    clone)
      if [[ -e "$REPOS/$url" ]];then
        telln 'Already cloned: `'$url'`'
        return 1
      fi
      mkdir -p "$ppath" # gitlab allows sub groups/directories
      git clone git@$server:$repo "$REPOS/$url"
      [[ $? != 0 ]] && return 1
      cd "$REPOS/$url"
      echo "$REPOS/$url"
      repo mkmanifest
      ;;

    # Deletes the specified URL from the remote system and local $REPOS
    # directory. When called with no URL attempts to infer the repo of the
    # current working directory and deletes that repo instead changing
    # into the $REPOS directory when completed.

    delete)
      if [[ "$server" == github.com ]]; then
        danger 'Do you really want to delete `'$server/$repo'`?'
        confirm || return
        repo forcedelete "$*"
        return
      fi
      telln 'Unsupported Git service or server: `'$server'`'
      return 1
      ;;

    # Deletes like `repo delete` but without any confirmation.

    forcedelete)
      if [[ "$server" == github.com ]]; then
        curl -X DELETE -H "Authorization: token $tok" \
          "https://api.github.com/repos/$repo"
        if [[ -d "$REPOS/$url" ]]; then
          cd "$REPOS"
          rm -rf "$REPOS/$url"
          repo mkmanifest
        fi
        return
      fi
      telln 'Unsupported Git service or server: `'$server'`'
      return 1
      ;;

    listremote)
      # TODO conditional on service github
      declare buf=$(curl -X GET -H "Authorization: token $tok" \
        "https://api.github.com/users/$GITUSER/repos" --silent)
      while IFS= read -r line; do
        line=${line//\"/}
        line=${line//\,/}
        line=${line##*:}
        echo $line
      done < <(echo "$buf" | perg full_name)
      return
      ;;

    listremoteother)
      declare buf=$(curl -X GET -H "Authorization: token $tok" \
        "https://api.github.com/user/repos" --silent)
      while IFS= read -r line; do
        line=${line//\"/}
        line=${line//\,/}
        line=${line##*:}
        echo $line
      done < <(echo "$buf" | perg full_name)
      return
      ;;

    # Forks the specified repo creating a repo in the user's remote
    # service collection and then clones it down into $REPOS locally (repo
    # clone).

    fork)
      if [[ "$server" == github.com ]]; then
        curl -X POST -H "Authorization: token $tok" -d '' "https://api.github.com/repos/$repo/forks" 
        repo clone "$*"
      fi
      return
      ;;

    private)
      telln 'Would make private'
      ;;

    public)
      curl -X PATCH -H "Authorization: token $tok" -d '{"private":false}' \
        "https://api.github.com/repos/$repo"
        return 
      ;;

    show)
      if [[ "$server" == github.com ]]; then
         curl -X GET -H "Authorization: token $tok" \
         "https://api.github.com/repos/$repo"
        return
      fi
      telln 'Unsupported Git service or server: `'$server'`'
      return 1
    ;;

    gobadges)
      echo "![WIP](https://img.shields.io/badge/status-wip-red)"
      echo "[![GoDoc](https://godoc.org/$url?status.svg)](https://godoc.org/$url)"
      echo "[![Go Report Card](https://goreportcard.com/badge/$url)](https://goreportcard.com/report/$url)"
      echo "[![Coverage](https://gocover.io/_badge/$url)](https://gocover.io/$url)"
      echo
      return
      ;;

    ping)
      declare server="${1%%/*}"
      ssh "git@$server"
      ;;

    # Uses the mapfile array loading trick to grab all the repos that
    # match the passed arguments. The only different between this and
    # match is that match just returns the match. This prompts for the
    # matching directory to change into if more than one, otherwise it
    # changes into it and echoes the new full path.

    *)
      mapfile -t  matches < <(repo match "$subcmd") # not really a subcmd
      if [[ ${#matches[@]} == 0 ]]; then
        telln 'No repos found.'
        return
      fi
      if [[ ${#matches[@]} == 1 ]]; then
        echo "$REPOS/${matches[0]}"
        cd "$REPOS/${matches[0]}"
        return
      fi
      select one in "${matches[@]}";do
        echo "$REPOS/$one"
        cd "$REPOS/$one"
        break
      done
      ;;

  esac
} && export -f repo && complete -W "list paths discover in mkmanifest
gobadges inpath match create clone delete forcedelete listremote
listremoteother fork private public show ping" repo

# TODO make aware of github as well so can be used for both

save () { 
    local y;
    local repo;
    local user=$(git config user.name);
    [[ -z "$user" ]] && echo "Git doesn't look configured yet." && return 1;
    git rev-parse > /dev/null 2>&1;
    if [[ ! $? = "0" ]]; then
        read -p "Not a git repo. Create? " y;
        if [[ $y =~ ^[yY] ]]; then
            touch README.md;
            read -p "GitLab path: " repo;
            git init;
            git remote add origin "git@gitlab.com:$repo.git";
            git add -A .;
            git commit -a -m initial;
            git push -u origin master;
        fi;
        return 0;
    fi;
    if [[ -z "$(git status -s)" && $(git rev-list --count origin/master..master) = 0 ]]; then
        echo Already at the latest.;
        return 0;
    fi;
    local comment=wip;
    [ ! -z "$*" ] && comment="$*";
    git pull;
    git add -A .;
    git commit -a -m "$comment";
    git push
}

#gh () {
#  declare auth="Authorization: token $(token gh)"
#  declare user=$(basepart $(dirpart $PWD))
#  declare repo=$(basepart $PWD)
#  [[ -n "$2" ]] && repo="$2"
#  [[ -n "$3" ]] && user="$3"
#  case "$1" in
#    create)
#      curl -X POST -H "$auth" -d '{"name":"'"$repo"'","private":true}' "https://api.github.com/user/repos"
#      gh init
#      ;;
#    show)
#      curl -X GET -H "$auth" "https://api.github.com/repos/$user/$repo"
#      ;;
#    clone)
#      mkdir -p ~/repos/$user
#      git clone git@github.com:$user/$repo ~/repos/$user/$repo
#      cd ~/repos/$user/$repo
#      ;;
#    fork)
#      curl -X POST -H "$auth" -d '' "https://api.github.com/repos/$user/$repo/forks" 
#      ;;
#    private)
#      curl -X PATCH -H "$auth" -d '{"private":true}' "https://api.github.com/repos/$user/$repo"
#      ;;
#    public)
#      curl -X PATCH -H "$auth" -d '{"private":false}' "https://api.github.com/repos/$user/$repo"
#      ;;
#    ping)
#      ssh git@github.com
#      ;;
#    init)
#      [[ -d .git ]] && warnln 'Already has a `.git`.' && return
#      touch README.md
#      git init
#      git add README.md
#      git commit -m init
#      git remote add origin "git@github.com:$user/$repo"
#      git push -u origin master
#      ;;
#    delete)
#      danger 'Do you really want to delete `'"$user/$repo"'`?' || return
#      curl -X DELETE -H "$auth" "https://api.github.com/repos/$user/$repo"
#      rm -rf "$HOME/repos/$repo"
#      rm -rf "$HOME/go/src/github.com/$user/$repo"
#      ;;
#    gobadges)
#      echo "![WIP](https://img.shields.io/badge/status-wip-red)"
#      echo "[![GoDoc](https://godoc.org/github.com/$user/$repo?status.svg)](https://godoc.org/github.com/$user/$repo)"
#      echo "[![Go Report Card](https://goreportcard.com/badge/github.com/$user/$repo)](https://goreportcard.com/report/github.com/$user/$repo)"
#      echo "[![Coverage](https://gocover.io/_badge/github.com/$user/$repo)](https://gocover.io/github.com/$user/$repo)"
#      echo
#      ;;
#    *)
#      cd ~/repos/github.com/$(git config user.name)
#      ;;
#  esac
#} && export -f gh && complete -W "create show clone fork init delete private public gobadges ping" gh
#

# Passes the arguments on to curl calling the GitLab GraphQL API. Use to
# construct specific service calls.
    #-d '{"query": "'$q'"}' \

curlgl () {
  declare q=$(argsorin $*)
  echo "$q"
  return
  curl -X POST \
    -H "Authorization: Bearer $(token gitlab.com)" \
    -H 'Content-Type: application/json' \
    -d "{\"query\": \"$q\"}" \
    'https://gitlab.com/api/graphql'
  return $?
} && export -f curlgl

lab () {
  case "$1" in
    private)
      echo would change to private;
    ;;
  *)
    echo unimplemented;
    ;;
  esac
} && export -f lab


################## Vim Editing / Magic Wand (!) Functions ################

# Fast way to pull down newly added Plug plugins after update to .vimrc.
# When in doubt begin names with the letter 'p' for disambiguation.

# TODO make a want function that converts Pandoc tables (which are so easy
# to make) into the GitHub Flavored Markdown style tables (that are
# supported everywhere). Need something like that for README.md files that
# are primarily read on GitHub and GitLab instead of getting converted to
# HTML, LaTeX or whatever (which Pandoc focuses on).

alias vimpluginstall="vim +':PlugInstall' +':q!' +':q!'"

# TODO Set the VIMSPELL env variable pointing to the main `add` file so editing

funcsin () {
  egrep '^[-_[:alpha:]]* ?\(' $1 | while read line; do
    echo ${line%%[ (]*}
  done
} && export -f funcsin

aliasesin () {
  mapfile -t  lines < <(perg '^alias ' "$*")
  declare line
  # LEFTOFF
  for line in "${lines[@]}"; do
    echo $line
  done
} && export -f aliasesin

# Opens any executable found in the PATH for editing, including binaries.

vic () {
  vi $(which $1)
} && export -f vic

# Echo the first argument, second argument (n) number of times.

echon () {
  declare i
  for ((i=0; i<${2:-1}; i++)); do echo -n "$1"; done
} && export -f echon

export HRULEWIDTH=74

hrule () {
  declare ch="${1:-#}"
  echo $(echon "$ch" $HRULEWIDTH)
} && export -f hrule

# This is how all the heading separators are made (!!htitle Title)

htitle () {
  declare str=$(argsorin $*)
  declare len=${#str}
  declare side=$(( ((HRULEWIDTH/2)-len/2)-1 ))
  declare left=$side
  declare right=$side
  [[ $[len%2] == 1 ]] && right=$[right-1]
  echo "$(echon '#' $left) $str $(echon '#' $right)"
} && export -f htitle

h1now () { now '#'; } && export -f h1now
h2now () { now '##'; } && export -f h2now
h3now () { now '###'; } && export -f h3now
h4now () { now '####'; } && export -f h4now
h5now () { now '#####'; } && export -f h5now
h6now () { now '######'; } && export -f h6now

######################### Lynx Text Browser FTW! #########################

# This version of lynx does not allow for anything to be passed to it but
# the URL it is to search for. All other configurations must be placed in
# the ~/.config/lynx/lynx.cfg and ~/.config/lynx/lynx.lss files. 

if [ -e "$HOME/.config/lynx/lynx.cfg" ];then
  export LYNX_CFG="$HOME/.config/lynx/lynx.cfg"
fi

if [ -e "$HOME/.config/lynx/lynx.lss" ];then
  export LYNX_LSS="$HOME/.config/lynx/lynx.lss"
fi

export _lynx=$(which lynx)

lynx () { 
  if [ -z "$_lynx" ]; then
      echo "Doesn't look like lynx is installed."
      return 1
  fi
  $_lynx $*
} && export -f lynx 


############################# Personal Status ############################

# This stuff if for maintaining your personal status indicators wherere'
# they may be (GitHub, GitLab, Twitter, Twitch (on stream), IRC, Discord,
# etc.) These are in the Bash config because they are closer to the other
# command functions that will be called to change the status. For example,
# when the `blog` command function is called it can directly include calls
# to the following to change the status detecting it automatically. Vim
# can also be setup to change personal status based on what you are coding
# on. The personal status can be tied to things like Discord that detect
# what "game" you are playing.


schedule () {
  open $SCHEDULE
} && export -f schedule

################### README World Exchange / WorldPress ###################

# This is stuff being prototyped for inclusion into the `rw` tool (which
# is written in Golang).

rw () {

  if [[ -z "$README" ]]; then
    telln 'Need to define `$README` directory.'
    return 1
  fi

  declare _rw=$(which rw)
  declare action="${1:-cd}"; shift

  case "$action" in 

    # Change into the current `$README` directory or module within it. If
    # a specific match is not found searches from a list of all modules
    # and prompts to select one.

    cd|d|dir)
      # TODO read first argument, look for
      cd "$README"
      ;;

    week)
      echo "$(year)w$(week)"
      ;;

    cal)
      # TODO in final version include the ncal code equiv
      # rather than depend on n/cal being on system.
      declare exe=$(which ncal)
      if [[ -z "$exe" ]]; then
        cal
      fi
      $exe -M -w
      ;;

    blog)
      declare blogdir="$README/$(rw week)"
      [[ ! -d $blogdir ]] && mkdir -p $blogdir
      $EDITOR $blogdir/README.md
      tell "Would you like to commit?"
      # TODO factor this into a save)
      if confirm; then
          cd $README
          save
          cd -
      fi
      ;;

    *)
      usageln 'rw <subcommand> <argument>...'
      # TODO later pass off to _rw
      ;;

  esac
} && export -f rw && complete -W 'cd d dir blog' rw

# Imma go ahead and create a `blog` command function to go with the `rw
# blog` subcommand only because that is what I would be doing once `rw` is
# released as a standalone package.

blog () {
  if havecmd rw; then
    rw blog $*
  fi
} && export -f blog 

################### Simple Todo Utility Using Markdown ###################

todo () {
  if [[ -n "$TODOFILE" ]]; then
    $EDITOR $TODOFILE
  else
    telln 'No `$TODOFILE` set.' 
  fi
} && export -f todo

############################# Go Development #############################

# Not being able to use private repos by default with Go is really
# annoying. This is the standard way to overcome that.

export GOPRIVATE="github.com/$GITUSER/*,gitlab.com/$GITUSER/*"
export GOBIN="$HOME/go/bin"
mkdir -p $GOBIN

# Changes into the first Go directory (from GOPATH) matching the name
# passed by looking up the package by specifying the ending string of the
# package name. Prompts for selection if more than one match.  Useful for
# quickly examining the source code of any Go package on the local system.

# (Use of gocd is largely deprecated along with "GOPATH mode" in general.
# Instead keep your go code with all your other repos and use "module
# mode" with `replace` when needed.

gocd () {
  declare q="$1"
  declare list=$(go list -f '{{.Dir}}' ...$q 2>/dev/null)
  IFS=$'\n' declare lines=($list) # split lines
  case "${#lines}" in
    0) tell 'Nothing found for "`'$q'`"' ;;
    1) cd $n ;;
    *) select path in "${lines[@]}"; do cd $path; break; done ;;
  esac
}

# Can be run from TMUX pane to simply watch the tests without having to
# explicitely run them constantly. When combined with a dump() Go utility
# function provides immediate, real-time insight without complicating
# development with a bloated IDE. (Also see Monitoring for other ideas for
# running real-time evaluations during development)

alias goi='go install'
alias gor='go run main.go'
alias got='go test'

gott () {
  declare seconds="$1"
  while true; do 
    go test
    sleep ${seconds:-3}
  done
}

# Builds the passed Go package or command for every supported operating
# system and architecture into directories named for such and logs.

godistbuild () {
  declare relpath="${1-.}"
  declare log="$PWD/build.log"
  >| $log # truncate
  for dist in $(go tool dist list); do
    [[ ! -d $dist ]] && mkdir -p $dist
      declare os=${dist%/*}
      declare arch=${dist#*/}
      echo "BUILDING: $os-$arch" |tee -a $log
      cd $dist
      GOOS=$os GOARCH=$arch go build $relpath  >> $log 2>&1
      echo >> $log
      cd - &>/dev/null
  done
}
# demolish any --user installed cabal packages.
cabalwipe() {
    rm -rf "$HOME/.cabal/packages"/*/*
    rm -rf "$HOME/.cabal/bin"/*
    rm -rf "$HOME/.ghc"
}

# filegrep 'foo.*' ./some/dir, greps all files in the given dir for the
# given regex
filegrep() {
    local dir="$2" regex="$1"
    find "$dir" -type f ! -wholename '*/.svn/*' ! -wholename '*/.git/*' -exec grep --color=auto -- "$regex" {} \+
}

# combine pdfs into one using ghostscript
combinepdf() {
    _have gs       || return 1
    [[ $# -ge 2 ]] || return 1

    local out="$1"; shift

    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile="$out" "$@"
}

# add by artist to mpc
addartist() {
    _have mpc || return 1

    mpc search artist "$*" | mpc add &>/dev/null
    mpc play
}

# make a thumb %20 the size of a pic
thumbit() {
    _have mogrify || return 1

    for pic; do
        case "$pic" in
            *.jpg)  thumb="${pic/.jpg/-thumb.jpg}"   ;;
            *.jpeg) thumb="${pic/.jpeg/-thumb.jpeg}" ;;
            *.png)  thumb="${pic/.png/-thumb.png}"   ;;
            *.bmp)  thumb="${pic/.bmp/-thumb.bmp}"   ;;
        esac

        [[ -z "$thumb" ]] && return 1

        cp "$pic" "$thumb" && mogrify -resize 10% "$thumb"
    done
}

# rip a dvd with handbrake
hbrip() {
    _have HandBrakeCLI || return 1
    [[ -n "$1" ]]      || return 1

    local name="$1" out drop="$HOME/Videos"; shift
    [[ -d "$drop" ]] || mkdir -p "$drop"

    out="$drop/$name.m4v"

    echo "rip /dev/sr0 --> $out"
    HandBrakeCLI --main-feature -m -s scan -F -N eng -Z High Profile "$@" -i /dev/sr0 -o "$out" 2>/dev/null
    echo
}

# convert media to ipad format with handbrake
hbconvert() {
    _have HandBrakeCLI || return 1
    [[ -n "$1" ]]      || return 1

    local in="$1" out drop="$HOME/Videos/converted"; shift
    [[ -d "$drop" ]] || mkdir -p "$drop"

    out="$drop/$(basename "${in%.*}").mp4"

    echo "convert $in --> $out"
    HandBrakeCLI -Z iPad "$@" -i "$in" -o "$out" 2>/dev/null
    echo
}

# simple spellchecker, uses /usr/share/dict/words
spellcheck() {
    [[ -f /usr/share/dict/words ]] || return 1

    for word; do
        if grep -Fqx "$word" /usr/share/dict/words; then
            echo -e "\e[1;32m$word\e[0m" # green
        else
            echo -e "\e[1;31m$word\e[0m" # red
        fi
    done
}

# go to google for anything
google() {
    [[ -z "$BROWSER" ]] && return 1

    local term="${*:-$(xclip -o)}"

    $BROWSER "http://www.google.com/search?q=${term// /+}" &>/dev/null &
}

# go to google for a definition
define() {
    _have w3m     || return 1
    _have mpv || return 1

    local word="$*"

    w3m -dump "http://www.google.com/search?q=define%20${word// /_}" | awk '/^     1./,/^        More info >>/'
    mpv "http://ssl.gstatic.com/dictionary/static/sounds/de/0/${word// /_}.mp3" &>/dev/null
}

# grep by paragraph
grepp() { perl -00ne "print if /$1/i" < "$2"; }

# pull a single file out of an achive, stops on first match. useful for
# .PKGINFO files in .pkg.tar.[gx]z files.
pullout() {
    _have bsdtar || return 1

    local opt

    case "$2" in
        *gz) opt='-qxzf' ;;
        *xz) opt='-qxJf' ;;
        *)   return 1    ;;
    esac

    bsdtar $opt "$2" "$1"
}

# recursively 'fix' dir/file perm
fix() {
    local dir

    for dir; do
        find "$dir" -type d -exec chmod 755 {} \;
        find "$dir" -type f -exec chmod 644 {} \;
    done
}

# print docs to default printer in reverse page order
printr() {
    _have enscript || return 1

    # stdin?
    if [[ -z "$*" ]]; then
        cat | enscript -p - | psselect -r | lp
        return 0
    fi

    local file

    for file; do
        enscript -p - "$file" | psselect -r | lp
    done
}

# set an ad-hoc GUI timer
timer() {
    $_isxrunning || return 1
    _have zenity || return 1

    local N="${1:-5m}"; shift

    (sleep $N && zenity --info --title="Time's Up" --text="${*:-DING}") &
    echo "timer set for $N"
}

# send an attachment from CLI
send() {
    _have mutt    || return 1
    [[ -f "$1" ]] || return 1
    [[ -z "$2" ]] || return 1

    echo 'Please see attached.' | mutt -s "File: $1" -a "$1" -- "$2"
}

# run a bash script in 'debug' mode
debug() {
    local script="$1"; shift

    if _have "$script"; then
        PS4='+$LINENO:$FUNCNAME: ' bash -x "$script" "$@"
    fi
}

# go to a directory or file's parent
goto() { [[ -d "$1" ]] && cd "$1" || cd "$(dirname "$1")"; }

# copy and follow
cpf() { cp "$@" && goto "$_"; }

# move and follow
mvf() { mv "$@" && goto "$_"; }

# print the url to a manpage
webman() { echo "http://unixhelp.ed.ac.uk/CGI/man-cgi?$1"; }

# Simple calculator
calc() {
	  local result=""
	  result="$(printf "scale=10;$*\n" | bc --mathlib | tr -d '\\\n')"
	  #						└─ default (when `--mathlib` is used) is 20

	  if [[ "$result" == *.* ]]; then
		    # improve the output for decimal numbers
		    printf "$result" |
		        sed -e 's/^\./0./'		  `# add "0" for cases like ".5"` \
			          -e 's/^-\./-0./'	  `# add "0" for cases like "-.5"`\
			          -e 's/0*$//;s/\.$//';  # remove trailing zeros
	  else
		    printf "$result"
	  fi
	  printf "\n"
}

# Create a new directory and enter it
mkd() {
	  mkdir -p "$@" && cd "$@"
}

# Make a temporary directory and enter it
tmpd() {
	  if [ $# -eq 0 ]; then
		    dir=`mktemp -d` && cd $dir
	  else
		    dir=`mktemp -d -t $1.XXXXXXXXXX` && cd $dir
	  fi
}

# Create a .tar.gz archive, using `zopfli`, `pigz` or `gzip` for compression
targz() {
	  local tmpFile="${@%/}.tar"
	  tar -cvf "${tmpFile}" --exclude=".DS_Store" "${@}" || return 1

	  size=$(
	      stat -f"%z" "${tmpFile}" 2> /dev/null; # OS X `stat`
	      stat -c"%s" "${tmpFile}" 2> /dev/null # GNU `stat`
	      )

	  local cmd=""
	  if (( size < 52428800 )) && hash zopfli 2> /dev/null; then
		    # the .tar file is smaller than 50 MB and Zopfli is available; use it
		    cmd="zopfli"
	  else
		    if hash pigz 2> /dev/null; then
			      cmd="pigz"
		    else
			      cmd="gzip"
		    fi
	  fi

	  echo "Compressing .tar using \`${cmd}\`…"
	  "${cmd}" -v "${tmpFile}" || return 1
	  [ -f "${tmpFile}" ] && rm "${tmpFile}"
	  echo "${tmpFile}.gz created successfully."
}

# Determine size of a file or total size of a directory
fs() {
	  if du -b /dev/null > /dev/null 2>&1; then
		    local arg=-sbh
	  else
		    local arg=-sh
	  fi
	  if [[ -n "$@" ]]; then
		    du $arg -- "$@"
	  else
		    du $arg .[^.]* *
	  fi
}

# Create a data URL from a file
dataurl() {
	  local mimeType=$(file -b --mime-type "$1")
	  if [[ $mimeType == text/* ]]; then
		    mimeType="${mimeType};charset=utf-8"
	  fi
	  echo "data:${mimeType};base64,$(openssl base64 -in "$1" | tr -d '\n')"
}

# Create a git.io short URL
gitio() {
	  if [ -z "${1}" -o -z "${2}" ]; then
		    echo "Usage: \`gitio slug url\`"
		    return 1
	  fi
	  curl -i http://git.io/ -F "url=${2}" -F "code=${1}"
}

# Start an HTTP server from a directory, optionally specifying the port
server() {
	  local port="${1:-8000}"
	  sleep 1 && open "http://localhost:${port}/" &
	  # Set the default Content-Type to `text/plain` instead of `application/octet-stream`
	  # And serve everything as UTF-8 (although not technically correct, this doesn’t break anything for binary files)
	  python -c $'import SimpleHTTPServer;\nmap = SimpleHTTPServer.SimpleHTTPRequestHandler.extensions_map;\nmap[""] = "text/plain";\nfor key, value in map.items():\n\tmap[key] = value + ";charset=UTF-8";\nSimpleHTTPServer.test();' "$port"
}

# Compare original and gzipped file size
gz() {
	  local origsize=$(wc -c < "$1")
	  local gzipsize=$(gzip -c "$1" | wc -c)
	  local ratio=$(echo "$gzipsize * 100 / $origsize" | bc -l)
	  printf "orig: %d bytes\n" "$origsize"
	  printf "gzip: %d bytes (%2.2f%%)\n" "$gzipsize" "$ratio"
}

# Syntax-highlight JSON strings or files
# Usage: `json '{"foo":42}'` or `echo '{"foo":42}' | json`
json() {
	  if [ -t 0 ]; then # argument
		    python -mjson.tool <<< "$*" | pygmentize -l javascript
	  else # pipe
		    python -mjson.tool | pygmentize -l javascript
	  fi
}

# Run `dig` and display the most useful info
digga() {
	  dig +nocmd "$1" any +multiline +noall +answer
}

# Query Wikipedia via console over DNS
mwiki() {
	  dig +short txt "$*".wp.dg.cx
}

# UTF-8-encode a string of Unicode symbols
escape() {
	  printf "\\\x%s" $(printf "$@" | xxd -p -c1 -u)
	  # print a newline unless we’re piping the output to another program
	  if [ -t 1 ]; then
		    echo ""; # newline
	  fi
}

# Decode \x{ABCD}-style Unicode escape sequences
unidecode() {
	  perl -e "binmode(STDOUT, ':utf8'); print \"$@\""
	  # print a newline unless we’re piping the output to another program
	  if [ -t 1 ]; then
		    echo ""; # newline
	  fi
}

# Get a character’s Unicode code point
codepoint() {
	  perl -e "use utf8; print sprintf('U+%04X', ord(\"$@\"))"
	  # print a newline unless we’re piping the output to another program
	  if [ -t 1 ]; then
		    echo ""; # newline
	  fi
}

# Show all the names (CNs and SANs) listed in the SSL certificate
# for a given domain
getcertnames() {
	  if [ -z "${1}" ]; then
		    echo "ERROR: No domain specified."
		    return 1
	  fi

	  local domain="${1}"
	  echo "Testing ${domain}…"
	  echo ""; # newline

	  local tmp=$(echo -e "GET / HTTP/1.0\nEOT" \
		                   | openssl s_client -connect "${domain}:443" 2>&1)

	  if [[ "${tmp}" = *"-----BEGIN CERTIFICATE-----"* ]]; then
		    local certText=$(echo "${tmp}" \
			                          | openssl x509 -text -certopt "no_header, no_serial, no_version, \
			no_signame, no_validity, no_issuer, no_pubkey, no_sigdump, no_aux")
		    echo "Common Name:"
		    echo ""; # newline
		    echo "${certText}" | grep "Subject:" | sed -e "s/^.*CN=//"
		    echo ""; # newline
		    echo "Subject Alternative Name(s):"
		    echo ""; # newline
		    echo "${certText}" | grep -A 1 "Subject Alternative Name:" \
			      | sed -e "2s/DNS://g" -e "s/ //g" | tr "," "\n" | tail -n +2
		    return 0
	  else
		    echo "ERROR: Certificate not found."
		    return 1
	  fi
}

# `v` with no arguments opens the current directory in Vim, otherwise opens the
# given location
v() {
	  if [ $# -eq 0 ]; then
		    vim .
	  else
		    vim "$@"
	  fi
}

# `o` with no arguments opens the current directory, otherwise opens the given
# location
o() {
	  if [ $# -eq 0 ]; then
		    xdg-open .	> /dev/null 2>&1
	  else
		    xdg-open "$@" > /dev/null 2>&1
	  fi
}

# `tre` is a shorthand for `tree` with hidden files and color enabled, ignoring
# the `.git` directory, listing directories first. The output gets piped into
# `less` with options to preserve color and line numbers, unless the output is
# small enough for one screen.
tre() {
	  tree -aC -I '.git' --dirsfirst "$@" | less -FRNX
}

# Call from a local repo to open the repository on github/bitbucket in browser
repo() {
	  local giturl=$(git config --get remote.origin.url | sed 's/git@/\/\//g' | sed 's/.git$//' | sed 's/https://g' | sed 's/:/\//g')
	  if [[ $giturl == "" ]]; then
		    echo "Not a git repository or no remote.origin.url is set."
	  else
		    local gitbranch=$(git rev-parse --abbrev-ref HEAD)
		    local giturl="http:${giturl}"

		    if [[ $gitbranch != "master" ]]; then
			      if echo "${giturl}" | grep -i "bitbucket" > /dev/null ; then
				        local giturl="${giturl}/branch/${gitbranch}"
			      else
				        local giturl="${giturl}/tree/${gitbranch}"
			      fi
		    fi

		    echo $giturl
		    open $giturl
	  fi
}

# Get colors in manual pages
man() {
	  env \
		    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
		    LESS_TERMCAP_md=$(printf "\e[1;31m") \
		    LESS_TERMCAP_me=$(printf "\e[0m") \
		    LESS_TERMCAP_se=$(printf "\e[0m") \
		    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
		    LESS_TERMCAP_ue=$(printf "\e[0m") \
		    LESS_TERMCAP_us=$(printf "\e[1;32m") \
		    man "$@"
}

# Use feh to nicely view images
openimage() {
	  local types='*.jpg *.JPG *.png *.PNG *.gif *.GIF *.jpeg *.JPEG'

	  cd $(dirname "$1")
	  local file=$(basename "$1")

	  feh -q $types --auto-zoom \
		    --sort filename --borderless \
		    --scale-down --draw-filename \
		    --image-bg black \
		    --start-at "$file"
}

# get dbus session
dbs() {
	  local t=$1
	  if [[  -z "$t" ]]; then
		    local t="session"
	  fi

	  dbus-send --$t --dest=org.freedesktop.DBus \
		          --type=method_call	--print-reply \
		          /org/freedesktop/DBus org.freedesktop.DBus.ListNames
}

# check if uri is up
isup() {
	  local uri=$1

	  if curl -s --head  --request GET "$uri" | grep "200 OK" > /dev/null ; then
		    notify-send --urgency=critical "$uri is down"
	  else
		    notify-send --urgency=low "$uri is up"
	  fi
}

# build go static binary from root of project
gostatic(){
	  local dir=$1
	  local arg=$2

	  if [[ -z $dir ]]; then
		    dir=$(pwd)
	  fi

	  local name=$(basename "$dir")
	  (
	      cd $dir
	      export GOOS=linux
	      echo "Building static binary for $name in $dir"

	      case $arg in
		        "netgo")
			          set -x
			          go build -a \
				           -tags 'netgo static_build' \
				           -installsuffix netgo \
				           -ldflags "-w" \
				           -o "$name" .
			          ;;
		        "cgo")
			          set -x
			          CGO_ENABLED=1 go build -a \
				                   -tags 'cgo static_build' \
				                   -ldflags "-w -extldflags -static" \
				                   -o "$name" .
			          ;;
		        *)
			          set -x
			          CGO_ENABLED=0 go build -a \
				                   -installsuffix cgo \
				                   -ldflags "-w" \
				                   -o "$name" .
			          ;;
	      esac
	  )
}

# go to a folder easily in your gopath
gogo(){
	  local d=$1

	  if [[ -z $d ]]; then
		    echo "You need to specify a project name."
		    return 1
	  fi

	  if [[ "$d" == github* ]]; then
		    d=$(echo $d | sed 's/.*\///')
	  fi
	  d=${d%/}

	  # search for the project dir in the GOPATH
	  local path=( `find "${GOPATH}/src" \( -type d -o -type l \) -iname "$d"  | awk '{print length, $0;}' | sort -n | awk '{print $2}'` )

	  if [ "$path" == "" ] || [ "${path[*]}" == "" ]; then
		    echo "Could not find a directory named $d in $GOPATH"
		    echo "Maybe you need to 'go get' it ;)"
		    return 1
	  fi

	  # enter the first path found
	  cd "${path[0]}"
}

golistdeps(){
	  (
	      if [[ ! -z "$1" ]]; then
		        gogo $@
	      fi

	      go list -e -f '{{join .Deps "\n"}}' ./... | xargs go list -e -f '{{if not .Standard}}{{.ImportPath}}{{end}}'
	  )
}

# get the name of a x window
xname(){
	  local window_id=$1

	  if [[ -z $window_id ]]; then
		    echo "Please specifiy a window id, you find this with 'xwininfo'"

		    return 1
	  fi

	  local match_string='".*"'
	  local match_int='[0-9][0-9]*'
	  local match_qstring='"[^"\\]*(\\.[^"\\]*)*"' # NOTE: Adds 1 backreference

	  # get the name
	  xprop -id $window_id | \
		    sed -nr \
		        -e "s/^WM_CLASS\(STRING\) = ($match_qstring), ($match_qstring)$/instance=\1\nclass=\3/p" \
		        -e "s/^WM_WINDOW_ROLE\(STRING\) = ($match_qstring)$/window_role=\1/p" \
		        -e "/^WM_NAME\(STRING\) = ($match_string)$/{s//title=\1/; h}" \
		        -e "/^_NET_WM_NAME\(UTF8_STRING\) = ($match_qstring)$/{s//title=\1/; h}" \
		        -e '${g; p}'
}

dell_monitor() {
	  xrandr --newmode "3840x2160_30.00"  338.75  3840 4080 4488 5136  2160 2163 2168 2200 -hsync +vsync
	  xrandr --addmode  DP1 "3840x2160_30.00"
	  xrandr --output eDP1 --auto --primary --output DP1 --mode 3840x2160_30.00 --above eDP1 --rate 30
}

shdl() { curl -O $(curl -s http://sci-hub.tw/"$@" | grep location.href | grep -o http.*pdf) ;}
se() { du -a ~/bin/* ~/.config/* | awk '{print $2}' | fzf | xargs -r $EDITOR ;}
sv() { vcopy "$(du -a ~/bin/* ~/.config/* | awk '{print $2}' | fzf)" ;}
vf() { fzf | xargs -r -I % $EDITOR % ;}

# Return the number of processors available
nproc() {
  if $(which nproc >/dev/null 2>&1); then
    command nproc
  elif [ -e /proc/cpuinfo ]; then
    grep ^processor < /proc/cpuinfo | wc -l
  else
    sysctl -n hw.ncpu
  fi
}
# is $1 installed?
_have() { which "$1" &>/dev/null; }

_islinux=false
[[ "$(uname -s)" =~ Linux|GNU|GNU/* ]] && _islinux=true

_ismac=false
[[ "$(uname -s)" =~ Darwin ]] && _ismac=true

_isubuntu=false
[[ "$(uname -v)" =~ Ubuntu ]] && _isubuntu=true
_isarch=false
[[ -f /etc/arch-release ]] && _isarch=true

_isxrunning=false
[[ -n "$DISPLAY" ]] && _isxrunning=true

_isroot=false
[[ $UID -eq 0 ]] && _isroot=true

# add directories to $PATH
_add_to_path() {
    local path

    for path; do
        [[ -d "$path" ]] && [[ ! ":${PATH}:" =~ :${path}: ]] && export PATH=${path}:$PATH
    done
}

# source a file if readable
_source () {
    local file="$1"
    [[ -r "$file" ]] || return 1
    . "$file"
}

_load_bash_completion_files() {
    FILES="$HOME/.bash_completion/*.bash"
    for config_file in $FILES
    do
        if [[ -e ${config_file} ]]; then
            source "$config_file"
        fi
    done
}

get_dpi() {
    r=$( (xrandr | \
             grep '\<connected\>' | \
             head -n 1 | \
             sed 's/[^0-9]/ /g' | \
             awk '{printf "%.0f\n", $2 / ($6 * 0.0394)}') \
           2>/dev/null)
    if [ -z "$r" ]; then
        echo 96  # fake it
    else
        echo "$r"
    fi
}
if $_islinux; then
    alias pbcopy='xclip -selection clipboard'
    alias pbpaste='xclip -selection clipboard -o'
fi

alias utc='env TZ="UTC" date'

# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ~="cd ~" # `cd` is probably faster to type though
alias -- -="cd -"

# Shortcuts
alias dl="cd ~/Downloads"
alias h="history"

if $_islinux; then
    alias um='udiskie-mount -r'
    alias uu='udiskie-umount'
fi

# tmux
if _have tmux; then
    alias t='tmux attach || tmux'
    alias txl='tmux ls'
    alias txn='tmux new -s'
    alias txa='tmux a -t'
fi

# only if we have mpc
if _have mpc; then
    alias addall='mpc --no-status clear && mpc listall | mpc --no-status add && mpc play'
    alias n='mpc next'
    alias p='mpc prev'
fi

if _have youtube-dl; then
    alias ytwlexp='youtube-dl --download-archive /mnt/SuperBig/Media/Video/YouTube/archive.txt --no-mtime --write-sub --sub-lang en_US --mark-watched --embed-subs -o "/mnt/SuperBig/Media/Video/YouTube/%(uploader)s-%(title)s.%(ext)s" https://www.youtube.com/playlist?list=PL5D8rBmak6B01Db6kNoGC7oNwL-nRoOFk'
    alias ytwl='youtube-dl --no-mtime --write-sub --sub-lang en_US --mark-watched --embed-subs -o "/mnt/SuperBig/Media/Video/YouTube/%(uploader)s-%(title)s.%(ext)s" :ytwatchlater'
    alias yt="youtube-dl --add-metadata -i"
    alias yta="yt -x -f bestaudio/best"
    alias YT="youtube-viewer"
    alias podcast='youtube-dl --no-mtime --verbose -x --audio-format mp3 --audio-quality 0 -o "/mnt/SuperBig/Media/Audio/podcast/%(uploader)s-%(upload_date)s-%(title)s.%(ext)s" https://www.youtube.com/playlist?list=PL5D8rBmak6B39tevwhz_LMTrEof7MeyQt'
fi

if _have rtorrent; then
    alias rtunlock="rm -f /mnt/SuperBig/Downloads/torrents/session/rtorrent.lock"
fi

# emacs
alias em='emacs'
alias en='emacs -nw'
alias et='emacsclient -t'
alias ed='emacs --daemon'
alias ec='emacsclient -c -a emacs'
function ekill() { emacsclient -e '(kill-emacs)';}

# vim
alias e='nvim'
alias E='sudo e'
alias f="vifm"

if _have git; then
    alias gc=". ${HOME}/.local/bin/gitdate && git commit -v "
    alias g="git"
    alias gaa="git aa"
    alias gd="git d"
    alias gdi="git di"
fi

if _have vagrant; then
    #vagrant
    alias vup="vagrant up"
    alias vh="vagrant halt"
    alias vs="vagrant suspend"
    alias vr="vagrant resume"
    alias vrl="vagrant reload"
    alias vssh="vagrant ssh"
    alias vst="vagrant status"
    alias vp="vagrant provision"
    alias vdstr="vagrant destroy"
    # requires vagrant-list plugin
    alias vl="vagrant list"
    # requires vagrant-hostmanager plugin
    alias vhst="vagrant hostmanager"
fi

# setup intellij idea
if [ -d $HOME/tools/idea ] ; then
    alias idea='$HOME/tools/idea/bin/idea.sh &'
fi

alias vnc='x11vnc -display :0 -forever -noxdamage -usepw -httpdir /usr/share/vnc-java/ -httpport 5800 &'
alias lisp='/usr/bin/sbcl'

#update dotfiles
if [ -d "$HOME/.dotfiles" ]; then
    alias ud='cd $HOME/.dotfiles && git pull origin master && rcup -v -x README.md && cd -'
fi

alias usage="du -skc * | sort -rn | more" #lists the sizes in blocks of all your files and sorts them in size order
alias total="du -ch | grep total" #find total size of directory and subdirectories
alias lsDir='ls -d */' #lisitng directories only
alias again="vim `ls -t | head -1`"
alias fixdirs="find . -type d -exec chmod o-rwx {} \;"
alias fixvideo="sudo chown -R dnewman:sambashare /mnt/SuperBig/Media/"
alias prev='ls -t | head -l'
alias findbig="find . -type f -exec ls -s {} \; | sort -n -r | head -5"
# safety features
alias cp='cp -i'
alias rm='rm -i'
alias mv='mv -i'
alias ln='ln -i'
alias tp="fzf --preview 'bat --color \"always\" {}'"
alias untar='tar xvf'


alias rm30='find * -mtime +30 -exec rm {} \;' # remove files older than 30 days
alias r="ranger"
alias sr="sudo ranger"
alias ka="killall"
alias bc='bc -l' # start calculator with math support
alias sha1='openssl sha1' # generate sha1 digest
alias ports='netstat -tulanp' # show open ports
alias mkdir='mkdir -pv'
alias tree='tree -L 2 -d -l'
alias tree2="find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'"
alias ccat="highlight --out-format=ansi" # Color cat - print file
alias c='clear'
alias h='history'
alias j='jobs -l'
alias nowtime='date +"%T"'
alias nowdate='date +"%d-%m-%Y"'
alias fastping='ping -c 100 -s .2'
if _have prettyping; then
    alias ping='prettyping --nolegend'
fi
alias ducks="du -sch .i[!.]* * |sort -rh |head -10" # print top 10 largest files in pwd
alias sps="ps aux | grep -v grep | grep"    # search and display processes by keyword
alias df='df -h'
if $_islinux; then
    alias grep='grep --color=auto'
fi
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias myip='printf "%s\n" "$(curl --silent http://tnx.nl/ip)"'
alias reload='exec bash'
alias ruler="echo .........1.........2.........3.........4.........5.........6.........7.........8"
alias psm="echo '%CPU %MEM   PID COMMAND' && ps hgaxo %cpu,%mem,pid,comm | sort -nrk1 | head -n 10 | sed -e 's/-bin//' | sed -e 's/-media-play//'"
alias blankcd="wodim -v dev=/dev/cdrw -blank=fast -eject"
alias count_files_recursive='find . -type f -print | wc -l'
alias count_files_recursive_per_directory='ls -d */ | xargs -I _ sh -c "find \"_\" -type f | wc -l | xargs echo _"'
alias flat_this_dir="sudo find . -mindepth 2 -type f -exec mv -i '{}' . ';'"
alias size_of_directory="ncdu --color dark -rr -x"
alias available_commands='bash -c "compgen -c"'

# Pipe my public key to my clipboard.
alias pubkey="more ~/.ssh/id_ed25519.pub | xclip -selection clipboard | echo '=> Public key copied to pasteboard.'"

# Pipe my private key to my clipboard.
alias prikey="more ~/.ssh/id_ed25519 | xclip -selection clipboard | echo '=> Private key copied to pasteboard.'"

# setup dual-montiors when plugged in
alias dual='xrandr --output LVDS-0 --auto --output HDMI-0 --auto --right-of LVDS-0'

if _have colortail; then
    alias tailirc='/usr/bin/colortail -q -k /etc/colortail/conf.irc'
    alias colortail='colortail -q -k /etc/colortail/conf.messages'
fi

# only if we have a disc drive
if [[ -b '/dev/sr0' ]]; then
    alias eject='eject -T /dev/sr0'
    alias mountdvd='sudo mount -t iso9660 -o ro /dev/sr0 /media/dvd/'
    alias backupdvd='dvdbackup -M -i /dev/sr0 -o ~/ripped/'
fi

# mount an iso
function mountiso() { sudo mount -t iso9660 -o loop "$@" /media/iso ;}

alias sdn="sudo shutdown now"
alias psref="gpg-connect-agent RELOADAGENT /bye" # Refresh gpg

# root aliases
if ! $_isroot; then
    alias svim='sudo vim'
    alias reboot='sudo reboot'
    alias shutdown='sudo shutdown -hP now'
    alias scat='sudo cat'
    alias root='sudo su'
    alias windows='sudo grub-set-default 3 && sudo reboot'
    alias bb="sudo bleachbit --clean system.cache system.localizations system.trash system.tmp"
fi

# ubuntu aliases
if $_isubuntu; then
    if ! $_isroot; then
        alias upgrade='sudo apt-get update && sudo apt-get upgrade -y'
        alias apt='sudo apt'
    fi
fi
alias SS="sudo systemctl"

# pacman aliases
if $_isarch; then
    if ! $_isroot; then
        _have pacman-color && alias pacman='sudo pacman-color' || alias pacman='sudo pacman'
        _have powerpill && alias powerpill='sudo powerpill'
    else
        _have pacman-color && alias pacman='pacman-color'
    fi
    _have yay && alias y='yay'
    alias p="sudo pacman"
    alias pacorphans='pacman -Rs $(pacman -Qtdq)'
    alias paccorrupt='sudo find /var/cache/pacman/pkg -name '\''*.part.*'\''' # sudo so we can quickly add -delete
    alias pactesting='pacman -Q $(pacman -Sql {community-,multilib-,}testing) 2>/dev/null'
    alias remove='sudo pacman -Rsc'
    alias pacin='pacman -S'
    alias pacout='pacman -R'
    alias pacsearch='pacman -Ss'
    alias pacup='pacup.rb && sudo pacman -Suy'
    alias upp='sudo reflector -l 20 --sort rate --save /etc/pacman.d/mirrorlist && cat /etc/pacman.d/mirrorlist && sudo pacman -Syyu && cd /etc/ && sudo etckeeper commit "Auto Commit"; cd -'
    # Pacman alias examples
    alias pacupg='sudo pacman -Syu'		# Synchronize with repositories and then upgrade packages that are out of date on the local system.
    alias pacdl='pacman -Sw'		# Download specified package(s) as .tar.xz ball
    alias pacin='sudo pacman -S'		# Install specific package(s) from the repositories
    alias pacins='sudo pacman -U'		# Install specific package not from the repositories but from a file
    alias pacre='sudo pacman -R'		# Remove the specified package(s), retaining its configuration(s) and required dependencies
    alias pacrem='sudo pacman -Rns'     	# Remove the specified package(s), its configuration(s) and unneeded dependencies
    alias pacrep='pacman -Si'		# Display information about a given package in the repositories
    alias pacreps='pacman -Ss'		# Search for package(s) in the repositories
    alias pacloc='pacman -Qi'		# Display information about a given package in the local database
    alias paclocs='pacman -Qs'		# Search for package(s) in the local database
    alias paclo="pacman -Qdt"		# List all packages which are orphaned
    alias pacc="sudo pacman -Scc"		# Clean cache - delete all the package files in the cache
    alias paclf="pacman -Ql"		# List all files installed by a given package
    alias pacown="pacman -Qo"		# Show package(s) owning the specified file(s)
    alias pacexpl="pacman -D --asexp"	# Mark one or more installed packages as explicitly installed
    alias pacimpl="pacman -D --asdep"	# Mark one or more installed packages as non explicitly installed

    # Additional pacman alias examples
    alias pacupd='sudo pacman -Sy && sudo abs'         # Update and refresh the local package and ABS databases against repositories
    alias pacinsd='sudo pacman -S --asdeps'            # Install given package(s) as dependencies
    alias pacmir='sudo pacman -Syy'                    # Force refresh of all package lists after updating /etc/pacman.d/mirrorlist

    # dealing with the following message from pacman:
    #
    #     error: couldnt lock database: file exists
    #     if you are sure a package manager is not already running, you can remove /var/lib/pacman/db.lck

    alias pacunlock="sudo rm /var/lib/pacman/db.lck"   # Delete the lock file /var/lib/pacman/db.lck
    alias paclock="sudo touch /var/lib/pacman/db.lck"  # Create the lock file /var/lib/pacman/db.lck

    # Show directory not owned by any package
    alias pacman-disowned-dirs="comm -23 <(sudo find / \( -path '/dev' -o -path '/sys' -o -path '/run' -o -path '/tmp' -o -path '/mnt' -o -path '/srv' -o -path '/proc' -o -path '/boot' -o -path '/home' -o -path '/root' -o -path '/media' -o -path '/var/lib/pacman' -o -path '/var/cache/pacman' \) -prune -o -type d -print | sed 's/\([^/]\)$/\1\//' | sort -u) <(pacman -Qlq | sort -u)"

    # Show files not owned by any packages
    alias pacman-disowned-files="comm -23 <(sudo find / \( -path '/dev' -o -path '/sys' -o -path '/run' -o -path '/tmp' -o -path '/mnt' -o -path '/srv' -o -path '/proc' -o -path '/boot' -o -path '/home' -o -path '/root' -o -path '/media' -o -path '/var/lib/pacman' -o -path '/var/cache/pacman' \) -prune -o -type f -print | sort -u) <(pacman -Qlq | sort -u)"

    alias lsp="pacman -Qett --color=always | less"

fi

# iptables
alias ipt='sudo /sbin/iptables'

# display all rules
alias iptlist='sudo /sbin/iptables -L -n -v --line-numbers'
alias iptlistin='sudo /sbin/iptables -L INPUT -n -v --line-numbers'
alias iptlistout='sudo /sbin/iptables -L OUTPUT -n -v --line-numbers'
alias iptlistfw='sudo /sbin/iptables -L FORWARD -n -v --line-numbers'
alias firewall=iptlist

# Debugging web servers
# get web server headers
alias header='curl -I'

# find out if remote server supports gzip or mod_deflate
alias headerc='curl -I --compress'

# get computer stats asap
alias meminfo='free -m -l -t'

#get top processes eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

#get top processes eating cpu
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

#get cpu info
alias cpuinfo='lscpu'

#get GPU ram
alias gpumeminfo='grep -i --color memory /var/log/Xorg.0.log'

# resume wget downloads by default
alias wget='wget -c'

alias top='atop'

# Common Hadoop File System Aliases
if _have hadoop; then
    alias hf="hadoop fs"                                         # Base Hadoop fs command
    alias hfcat="hf -cat"                                        # Output a file to standard out
    alias hfchgrp="hf -chgrp"                                    # Change group association of files
    alias hfchmod="hf -chmod"                                    # Change permissions
    alias hfchown="hf -chown"                                    # Change ownership
    alias hfcfl="hf -copyFromLocal"                              # Copy a local file reference to HDFS
    alias hfctl="hf -copyToLocal"                                # Copy a HDFS file reference to local
    alias hfcp="hf -cp"                                          # Copy files from source to destination
    alias hfdu="hf -du"                                          # Display aggregate length of files
    alias hfdus="hf -dus"                                        # Display a summary of file lengths
    alias hfget="hf -get"                                        # Get a file from hadoop to local
    alias hfgetm="hf -getmerge"                                  # Get files from hadoop to a local file
    alias hfls="hf -ls"                                          # List files
    alias hfll="hf -lsr"                                         # List files recursivly
    alias hfmkdir="hf -mkdir"                                    # Make a directory
    alias hfmv="hf -mv"                                          # Move a file
    alias hfput="hf -put"                                        # Put a file from local to hadoop
    alias hfrm="hf -rm"                                          # Remove a file
    alias hfrmr="hf -rmr"                                        # Remove a file recursivly
    alias hfsr="hf -setrep"                                      # Set the replication factor of a file
    alias hfstat="hf -stat"                                      # Returns the stat information on the path
    alias hftail="hf -tail"                                      # Tail a file
    alias hftest="hf -test"                                      # Run a series of file tests. See options
    alias hftouch="hf -touchz"                                   # Create a file of zero length

    # Convenient Hadoop File System Aliases
    alias hfet="hf -rmr .Trash"                                  # Remove/Empty the trash
    function hfdub() {                                           # Display aggregate size of files descending
        hadoop fs -du "$@" | sort -k 1 -n -r
    }

    #Common Hadoop Job Commands
    alias hj="hadoop job"                                        # Base Hadoop job command
    alias hjstat="hj -status"                                    # Print completion percentage and all job counters
    alias hjkill="hj -kill"                                      # Kills the job
    alias hjhist="hj -history"                                   # Prints job details, failed and killed tip details
    alias hjlist="hj -list"                                      # List jobs

    #Common Hadoop DFS Admin Commands
    alias habal="hadoop balancer"                                # Runs a cluster balancing utility
    alias harep="hadoop dfsadmin -report"                        # Print the hdfs admin report
fi

#Common Oozie Aliases/Functions
if _have oozie; then
    alias owv="oozie validate"                                   # Validate a workflow xml
    alias ojrun="oozie job -run"                                 # Run a job
    alias ojresume="oozie job -resume"                           # Resume a job
    alias ojrerun="oozie job -rerun"                             # Rerun a job
    alias ojsuspend="oozie job -suspend"                         # Suspend a job
    alias ojkill="oozie job -kill"                               # Kill a job
    alias ojinfo="oozie job -info"                               # Display current info on a job
    alias ojlist="oozie jobs -localtime"                         # Display a list of jobs
    alias ojlistr="oozie jobs -localtime -filter status=RUNNING" # Display a list of running jobs
fi

# Enable aliases to be sudo'ed
alias sudo='sudo '

if $_ismac; then
    alias grep='ggrep --color=auto'
    alias out="outdated_apps"
    alias up="update_apps"

    # Brew
    alias brwe="brew"
    alias bs="brew services"
    alias bsl="brew services list"
    alias bss="brew services start"
    alias bssp="brew services stop"
    alias bsr="brew services restart"
    alias bsspa="brew services stop --all"
    alias bi="brew_install"
    alias bu="brew_uninstall"
    alias bci="brew_cask_install"
    alias bcu="brew_cask_uninstall"
    alias i.='(idea $PWD &>/dev/null &)'
    alias o.='open .'
fi

alias publicip="dig +short myip.opendns.com @resolver1.opendns.com"
alias localip="sudo ifconfig | grep -Eo 'inet (addr:)?([0-9]*\\.){3}[0-9]*' | grep -Eo '([0-9]*\\.){3}[0-9]*' | grep -v '127.0.0.1'"
alias ips="sudo ifconfig -a | grep -o 'inet6\\? \\(addr:\\)\\?\\s\\?\\(\\(\\([0-9]\\+\\.\\)\\{3\\}[0-9]\\+\\)\\|[a-fA-F0-9:]\\+\\)' | awk '{ sub(/inet6? (addr:)? ?/, \"\"); print }'"

# Flush Directory Service cache
alias flush="dscacheutil -flushcache && killall -HUP mDNSResponder"

# View HTTP traffic
alias sniff="sudo ngrep -d 'en1' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -i en1 -n -s 0 -w - | grep -a -o -E \"Host\\: .*|GET \\/.*\""

# Canonical hex dump; some systems have this symlinked
command -v hd > /dev/null || alias hd="hexdump -C"

# OS X has no `md5sum`, so use `md5` as a fallback
command -v md5sum > /dev/null || alias md5sum="md5"

# OS X has no `sha1sum`, so use `shasum` as a fallback
command -v sha1sum > /dev/null || alias sha1sum="shasum"

# Trim new lines and copy to clipboard
alias c="tr -d '\\n' | xclip -selection clipboard"

# URL-encode strings
alias urlencode='python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1]);"'

# Merge PDF files
# Usage: `mergepdf -o output.pdf input{1,2,3}.pdf`
alias mergepdf='/System/Library/Automator/Combine\ PDF\ Pages.action/Contents/Resources/join.py'

# Intuitive map function
# For example, to list all directories that contain a certain file:
# find . -name .gitattributes | map dirname
alias map="xargs -n1"

# One of @janmoesen’s ProTip™s
for method in GET HEAD POST PUT DELETE TRACE OPTIONS; do
	# shellcheck disable=SC2139,SC2140
	alias "$method"="lwp-request -m \"$method\""
done

# Kill all the tabs in Chrome to free up memory
# [C] explained: http://www.commandlinefu.com/commands/view/402/exclude-grep-from-your-grepped-output-of-ps-alias-included-in-description
alias chromekill="ps ux | grep '[C]hrome Helper --type=renderer' | grep -v extension-process | tr -s ' ' | cut -d ' ' -f2 | xargs kill"

# vhosts
alias hosts='sudo vim /etc/hosts'

# copy working directory
alias cwd='pwd | tr -d "\r\n" | xclip -selection clipboard'

#Startup Zalenium docker container
alias startz='docker run --rm -ti --name zalenium -p 4444:4444 -v /var/run/docker.sock:/var/run/docker.sock -v /tmp/videos:/home/seluser/videos --privileged dosel/zalenium start --screenWidth 1920 --screenHeight 1080 --timeZone "America/Chicago"'
#alias startz='docker run --rm -ti --name zalenium -p 4444:4444 -e ZALENIUM_SELENIUM_CONTAINER_MEMORY_LIMIT=8589934592 -e ZALENIUM_SELENIUM_CONTAINER_CPU_LIMIT=400000000 -v /var/run/docker.sock:/var/run/docker.sock -v /tmp/videos:/home/seluser/videos --privileged dosel/zalenium start --screenWidth 1920 --screenHeight 1080 --timeZone "America/Chicago"'
# Kill running Zalenium containers
alias stopz='curl -sSL https://raw.githubusercontent.com/dosel/t/i/p | bash -s stop'

if _have curl; then
    alias cl='curl -L'
    # follow redirects, download as original name
    alias clo='curl -L -O'
    # follow redirects, download as original name, continue
    alias cloc='curl -L -C - -O'
    # follow redirects, download as original name, continue, retry 5 times
    alias clocr='curl -L -C - -O --retry 5'
    # follow redirects, fetch banner
    alias clb='curl -L -I'
    # see only response headers from a get request
    alias clhead='curl -D - -so /dev/null'
fi

#Maven
if _have mvn; then
    alias mci='mvn clean install'
    alias mi='mvn install'
    alias mrprep='mvn release:prepare'
    alias mrperf='mvn release:perform'
    alias mrrb='mvn release:rollback'
    alias mdep='mvn dependency:tree'
    alias mpom='mvn help:effective-pom'
    alias mcisk='mci -Dmaven.test.skip=true'
fi

#NordVPN
alias nord='sudo openvpn ~/Dropbox/nordvpn/us1967.nordvpn.com.udp1194.ovpn'

# Composer
if [ -f ~/bin/composer.phar ]; then
    alias composer='php ~/bin/composer.phar'
fi
# mac exports
if $_ismac; then
  export HOMEBREW_AUTO_UPDATE_SECS=86400;
  export HOMEBREW_NO_ANALYTICS=true;
  export HOMEBREW_INSTALL_BADGE="(ʘ‿ʘ)";
  export HOMEBREW_BUNDLE_FILE_PATH=~/.homebrew/Brewfile;
  export BASH_SILENCE_DEPRECATION_WARNING=1
fi

# For the fuzzy finder
if _have fzf; then
  export FZF_DEFAULT_OPTS='--color=bg+:24 --reverse';
fi

if [ -e ~/.dpi ]; then
  source ~/.dpi
fi

#Mac git completion
if $_ismac; then
  if [ -f `brew --prefix`/etc/bash_completion.d/git-completion.bash ]; then
    . `brew --prefix`/etc/bash_completion.d/git-completion.bash
  fi
fi

# Andriod SDK
if $_ismac; then
  if [ -d ~/Library/Android/sdk ]; then
    export ANDROID_SDK_ROOT="$HOME/Library/Android/sdk"
    _add_to_path "$HOME/Library/Android/sdk/platform-tools"
  fi
fi

# Ruby
if [[ -r "$HOME/.rbenv" ]]; then
    export PATH="$HOME/.rbenv/bin:$PATH"
    eval "$(rbenv init -)"
    export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
fi

#Composer
[[ -d "$HOME/.composer" ]] && _add_to_path "$HOME/.composer/vendor/bin"

# Heroku
[[ -r "/usr/local/heroku" ]] && _add_to_path "/usr/local/heroku/bin"

# PHP5
[[ -f /usr/local/php5/bin/php ]] && _add_to_path "/usr/local/php5/bin"

# MYSQL
[[ -f /usr/local/mysql/bin/mysql ]] && _add_to_path "/usr/local/mysql/bin"

#mac stuff
if $_ismac; then
  _add_to_path "/usr/local/sbin"
fi


# PyENV
if [[ -r "$HOME/.pyenv" ]]; then
  export PYENV_ROOT="$HOME/.pyenv"
  _add_to_path "$PYENV_ROOT/bin"
  if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
  fi
  eval "$(pyenv virtualenv-init -)"
  eval "$(register-python-argcomplete pipx)"
fi

#NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

###-tns-completion-start-###
if [ -f /home/dnewman/.tnsrc ]; then
  source /home/dnewman/.tnsrc
fi
###-tns-completion-end-###

[[ -r ~/.bash_personal   ]] && source ~/.bash_personal
[[ -r ~/.bash_private   ]] && source ~/.bash_private
