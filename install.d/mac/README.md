# Solarized Dark Mac Terminal Colors

Based on [tomislav's](https://github.com/tomislav/osx-terminal.app-colors-solarized) original terminal file but with the following minor modifications:

* Explicit 80x24 height and width set.
* Larger font size.
* More space between font characters.
